import React, {Component} from 'react';
import './App.css';
import {BrowserRouter as Router, Link, Redirect, Route, Switch,} from "react-router-dom";
import Home from './pages/manager/index';
import Message from './pages/message/index';
import Order from './pages/order/index';
import Portfolio from './pages/setting/portfolio/index';
import Report from './pages/report/index';
import Client from './pages/client/index';
import Login from './pages/login/index';
import Index from './pages/index';
import Address from './pages/setting/address/index';
import OurTeam from './pages/setting/ourteam/index';
import 'bootstrap/dist/css/bootstrap.min.css';
import {query} from "./service";
import history from "./history";
import Reconciliation from './pages/reconciliation/index';
import {toast} from "react-toastify";
import Collapsible from "./components/Collapsible";
import {library} from "@fortawesome/fontawesome-svg-core";
import {
    faAngleDown,
    faBalanceScale,
    faClone, faCogs,
    faDollarSign,
    faEnvelope,
    faFileInvoice, faHome, faIdCard,
    faInfoCircle, faMapMarkerAlt, faPeopleArrows, faReceipt, faSignOutAlt, faUserEdit,
    faUserPlus, faUsers, faUserTie,
    faUserTimes
} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faTrashAlt} from "@fortawesome/free-regular-svg-icons";
import { Spinner } from "reactstrap";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect: true,
            currentUser: '',
            loading: false
        }
    }
    async componentDidMount() {
        this.setState({ loading: true });
        const {data} = await query({
            path: '/api/user/me'
        });
        toast.configure({
            autoClose: 5000,
            draggable: false,
        });
        data.object &&
        this.setState({
            redirect: false,
            currentUser: data.object,
            loading: false
        });
        if (!data.success) {
            this.setState({
                redirect: true,
                loading: false
            });
        } else {
            if (localStorage.getItem('currentPage') === null) {
                history.push('/message');
            } else {
                history.push(localStorage.getItem('currentPage'));
            }
            this.setState({
                redirect: false,
                user: data.object,
                loading: false
            });
        }
    }

    render() {
        const logout = () => {
            window.location.reload();
            localStorage.removeItem('token');
            localStorage.removeItem('currentPage');
        };

        const handlePage = (param) => {
            localStorage.setItem('currentPage', param);
        };
        library.add(faEnvelope, faDollarSign, faFileInvoice, faUserTimes, faUserPlus, faInfoCircle, faTrashAlt, faBalanceScale,
            faReceipt, faClone, faUserEdit, faCogs, faAngleDown, faUsers, faIdCard, faMapMarkerAlt, faSignOutAlt, faHome, faPeopleArrows, faUserTie);

        if(this.state.loading) return (<div style={{display: 'flex', alignItems: 'center', justifyContent: 'center', height: '100vh'}}><Spinner type="grow" color="secondary" /></div>);

        return (
            <div className={"container-fluid"}>
                <Router>
                    {this.state.redirect ?
                        window.location.pathname === "/login" ? <Redirect to={"/login"}/> : <Redirect to='/'/> :
                        <Redirect to='/login'/>
                    }
                    {/*{*/}
                    {/*    this.state.currentUser.roles &&*/}
                    {/*    this.state.currentUser.roles.map(item => {*/}
                    {/*        return (*/}
                    {/*            item.roleName === "ROLE_DIRECTOR" ? <Redirect to={"/manager"}/> :*/}
                    {/*                <Redirect to={"/message"}/>*/}
                    {/*        )*/}
                    {/*    })*/}
                    {/*}*/}
                    {
                        this.state.redirect ?
                            <Switch>
                                <Route path="/login">
                                    <Login/>
                                </Route>
                                <Route path="/">
                                    <Index/>
                                </Route>
                            </Switch> :
                            <div>
                                <div className="row">
                                    <div className={"col-md-2 sider"}>
                                        <ul className="list-group admin">
                                            <li className={"list-group-item border-bottom font-weight-bold text-center"}>
                                                Euro print
                                            </li>
                                            <Link to={"/message"} smooth={true} duration={1500}
                                                  onClick={() => handlePage("/message")}>
                                                <li className={"list-group-item admin-item "}
                                                    style={{decoration: null}}><FontAwesomeIcon
                                                    icon={"envelope"}/>&nbsp;&nbsp; Xabarlar
                                                </li>
                                            </Link>
                                            <Link to={"/order"} onClick={() => handlePage("/order")}>
                                                <li className={"list-group-item admin-item "}><FontAwesomeIcon
                                                    icon={"file-invoice"}/>&nbsp;&nbsp;&nbsp;Buyurtmalar
                                                </li>
                                            </Link>
                                            <Link to={"/reconciliation"}
                                                  onClick={() => handlePage("/reconciliation")}>
                                                <li className={"list-group-item admin-item"}>
                                                    <FontAwesomeIcon icon={"balance-scale"}/>&nbsp;&nbsp;Aksverka
                                                </li>
                                            </Link>
                                            <div>
                                                <Collapsible trigger={<li className={"list-group-item admin-item"}>
                                                    <FontAwesomeIcon icon={"cogs"}/>&nbsp;&nbsp;<p
                                                    className={'d-inline-block mb-0'}
                                                    style={{fontSize: 18}}>Sozlamalar</p>
                                                </li>}
                                                             draggable={true}>
                                                    <Link to={"/ourteam"} onClick={() => handlePage("/ourteam")}>
                                                        <li className={"list-group-item admin-item text-decoration-none"}>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;<FontAwesomeIcon icon={"users"}/>
                                                            &nbsp;&nbsp;Bizning jamoa
                                                        </li>
                                                    </Link>
                                                    <Link to={"/portfolio"} onClick={() => handlePage("/portfolio")}>
                                                        <li className={"list-group-item admin-item"}>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;<FontAwesomeIcon
                                                            icon={"id-card"}/>&nbsp;&nbsp;&nbsp;Portfolio
                                                        </li>
                                                    </Link>
                                                    <Link to={"/address"} onClick={() => handlePage("/address")}>
                                                        <li className={"list-group-item admin-item mb-3"}>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;<FontAwesomeIcon
                                                            icon={"map-marker-alt"}/>&nbsp;&nbsp;&nbsp;&nbsp;Manzil
                                                        </li>
                                                    </Link>
                                                </Collapsible>
                                            </div>

                                            {this.state.currentUser.roles.map((item, index) =>
                                                item.roleName === "ROLE_DIRECTOR" &&
                                                <Link to={"/manager"} onClick={() => handlePage("/manager")}>
                                                    <li className={"list-group-item admin-item"}>
                                                        <FontAwesomeIcon icon={"user-tie"}/>&nbsp;&nbsp;&nbsp;Menejerlar
                                                    </li>
                                                </Link>
                                            )}
                                            <Link to={"/client"} onClick={() => handlePage("/client")}>
                                                <li className={"list-group-item admin-item"}>
                                                    <FontAwesomeIcon icon={"people-arrows"}/>&nbsp;&nbsp;Mijozlar
                                                </li>
                                            </Link>
                                            <Link to={"/report"} onClick={() => handlePage("/report")}>
                                                <li className={"list-group-item admin-item"}>
                                                    <FontAwesomeIcon icon={"receipt"}/>&nbsp;&nbsp;&nbsp;&nbsp;Reportlar
                                                </li>
                                            </Link>
                                            <li onClick={logout}
                                                className={"list-group-item admin-item"}>
                                                <FontAwesomeIcon icon={"home"}/>&nbsp;&nbsp;Bosh sahifa
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="col-md-10">
                                        <Switch history={history.location}>
                                            {this.state.currentUser.roles.map((item, index) =>
                                                item.roleName === "ROLE_DIRECTOR" &&
                                                <Route path="/manager">
                                                    <Home currentUser={this.state.currentUser}/>
                                                </Route>
                                            )}
                                            <Route path={"/reconciliation"}>
                                                <Reconciliation/>
                                            </Route>
                                            <Route path="/portfolio">
                                                <Portfolio/>
                                            </Route>
                                            <Route path="/message">
                                                <Message/>
                                            </Route>
                                            <Route path="/order">
                                                <Order/>
                                            </Route>
                                            <Route path="/address">
                                                <Address/>
                                            </Route>
                                            <Route path="/ourteam">
                                                <OurTeam/>
                                            </Route>
                                            <Route path="/client">
                                                <Client/>
                                            </Route>
                                            <Route path="/report">
                                                <Report/>
                                            </Route>
                                            <Route path="/login">
                                                <Home currentUser={this.state.currentUser}/>
                                            </Route>
                                        </Switch>
                                    </div>
                                </div>
                            </div>
                    }
                </Router>
            </div>
        );
    }
}

export default App;

