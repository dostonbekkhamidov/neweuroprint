import React, {Component} from 'react';
import uz from './locales/en-US';
import ru from './locales/ru-RU';

class FormatMessage extends Component {
    render() {
        return (
            <div>
                {localStorage.getItem("LANG")==="RU"? ru[this.props.id]:uz[this.props.id] }
            </div>
        );
    }
}

export default FormatMessage;

