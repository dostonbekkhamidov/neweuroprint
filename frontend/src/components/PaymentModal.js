import React from "react";
import {Modal, ModalHeader, ModalBody, ModalFooter, Button} from "reactstrap";
import {AvField, AvForm, AvInput} from "availity-reactstrap-validation";
import 'bootstrap/dist/css/bootstrap.min.css';

export const PaymentModal = ({
                                 payModalVisible,
                                 currentClient,
                                 clients,
                                 closePaymentModal,
                                 savePayment
                             }) => {

    return (
        <Modal isOpen={payModalVisible} toggle={closePaymentModal}>
            <ModalHeader toggle={closePaymentModal}>
                To'lov qo'shish.
            </ModalHeader>
            <ModalBody>
                <AvForm id={'form'} onSubmit={savePayment}>
                    <div className={"pt-0"}>
                        <div class={"row"}>
                            <div class={"col-md-6 mt-2"}>
                                <AvInput name={'amount'} type={'number'} placeholder={'Miqdori'} required/>
                            </div>
                            <div class={"col-md-6 p-0"}>
                                <AvField name={'payType'} required type={'select'} height={6}>
                                    <option disabled={true}>To'lov turi</option>
                                    <option>To'lov turi</option>
                                    <option value={'CASH'}>Naqd</option>
                                    <option value={'BANK'}>Bank</option>
                                    <option value={'CARD'}>Card</option>
                                </AvField>
                            </div>
                        </div>
                    </div>

                    <AvInput name={'description'} type={'text-area'} width={20} rows={4} placeholder={'Izox'}/>

                    {
                        currentClient ? ''
                            : <AvField name={'clientId'}>
                                {
                                    clients.map(item =>
                                        <option key={item.uuid} value={item.uuid}>
                                            {item.phoneNumber} &nbsp; {item.fullName}
                                        </option>)
                                }
                            </AvField>
                    }
                </AvForm>
            </ModalBody>
            <ModalFooter>
                <Button className={'btn btn-dark'} form={'form'} type={'submit'}> Saqlash </Button>
                <Button className={'btn btn-danger'} onClick={closePaymentModal}> Bekor qilish </Button>
            </ModalFooter>
        </Modal>
    )
};

export default PaymentModal;