import React, {Component} from 'react';
import {Carousel} from "react-bootstrap";

class Carusel extends Component {
    render() {
        return (
            <div className={"mt-5 pt-5"}>
                <Carousel class={"CaruselContainer"}>
                    <Carousel.Item>
                        <img
                            className="d-block ml-auto mr-auto h-50 img1 img-fluid"
                            src="/assets/images/group_1111.png"
                            alt="First slide"
                        />
                    </Carousel.Item>
                    <Carousel.Item>
                        <img
                            className="d-block  mr-auto h-50 img2 img-fluid"
                            src="/assets/images/group_2222.png"
                            alt="First slide"
                        />
                    </Carousel.Item>
                    <Carousel.Item>
                        <img
                            className="d-block ml-auto mr-auto h-50 img3 img-fluid"
                            src="/assets/images/group_3333.png"
                            alt="First slide"
                        />
                    </Carousel.Item>
                </Carousel>
            </div>
        );
    }
}

export default Carusel;