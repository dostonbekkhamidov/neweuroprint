import React, {Component} from 'react';
import '../../global.scss'
import {
    Container,
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
} from 'reactstrap';
import {FaAngleDown} from "react-icons/fa";
import FormattedMessage from '../FormatMessage';
import {Link} from "react-scroll/modules";
export default class Header extends Component {

    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    changeLang = (lang) => {
        window.location.reload(true);
        if (lang === 'UZ') {
            localStorage.setItem('LANG','UZ')
        } else {
            localStorage.setItem('LANG','RU')
        }
    };

    render() {
        const {scrolled, fluid} = this.props;
        return (
            <div className="">
                <Container className='p-0' style={{zIndex: "999"}}>
                    <Navbar light expand="md" className="lato-regular mr-0">
                        <NavbarBrand href="/"><img height={45} width={113} src="assets/images/logo.png"
                                                   alt="logo"/></NavbarBrand>
                        <NavbarToggler onClick={this.toggle}/>
                        <Collapse isOpen={this.state.isOpen} navbar>
                            <Nav className="ml-auto" navbar>
                                <NavItem>
                                    <Link onClick={this.toggle} spy={true} smooth={true} duration={1500}
                                          to="bizhaqimizda">
                                        <NavLink href="/" className="aboutUs">
                                            <FormattedMessage id="header_link1"/>
                                        </NavLink>
                                    </Link>
                                </NavItem>
                                <NavItem>
                                    <Link onClick={this.toggle} spy={true} smooth={true} duration={1500} to="xizmatlar">
                                        <NavLink href="/">
                                            <FormattedMessage id="header_link2"/>
                                        </NavLink>
                                    </Link>
                                </NavItem>
                                <NavItem>
                                    <Link onClick={this.toggle} spy={true} smooth={true} duration={1500} to="portfolio">
                                        <NavLink href="/">
                                            <FormattedMessage id="header_link3"/>
                                        </NavLink>
                                    </Link>
                                </NavItem>
                                <NavItem>
                                    <Link onClick={this.toggle} spy={true} smooth={true} duration={1500} to="mijozlar">
                                        <NavLink href="/">
                                            <FormattedMessage id="header_link4"/>
                                        </NavLink>
                                    </Link>
                                </NavItem>
                                <NavItem>
                                    <Link onClick={this.toggle} spy={true} smooth={true} duration={1500} to="jamoa">
                                        <NavLink href="/">
                                            <FormattedMessage id="header_link5"/>
                                        </NavLink>
                                    </Link>
                                </NavItem>
                                <NavItem>
                                    <Link onClick={this.toggle} spy={true} smooth={true} duration={1500} to="kontakt">
                                        <NavLink href="/">
                                            <FormattedMessage id="header_link6"/>
                                        </NavLink>
                                    </Link>
                                </NavItem>

                                <UncontrolledDropdown>
                                    <DropdownToggle caret className="dropdownMain">
                                        {localStorage.getItem("LANG") === "RU" ? "Русский" : "O`zbekcha"} <FaAngleDown
                                        className='faAngel'/>
                                    </DropdownToggle>
                                    <DropdownMenu right>
                                        <DropdownItem onClick={() => this.changeLang("UZ")} key={1}>
                                            <FormattedMessage id='header_item1'/>
                                        </DropdownItem>
                                        <DropdownItem onClick={() => this.changeLang("RU")} key={2}>
                                            <FormattedMessage id='header_item2'/>
                                        </DropdownItem>
                                    </DropdownMenu>
                                </UncontrolledDropdown>
                            </Nav>
                        </Collapse>
                    </Navbar>
                </Container>
            </div>
        );
    }
}
