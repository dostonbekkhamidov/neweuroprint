import React from 'react';
import {Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import {AvForm, AvInput} from "availity-reactstrap-validation";
import {generateForm} from './GenerateOrderForm';

const OrderModal = ({
                        orderVisibility,
                        saveOrder,
                        inputRow,
                        deleteRow,
                        addProduct,
                        closeOrderModal,
                        page,
                    }) => {

    const handleSubmit = (event, errors, values) => {
        if (errors.length === 0) {
            let products = generateForm(values);
            if (products.length !== 0) {
                saveOrder(products)
            }
        }
    };

    return (
        <Modal className={"orderModal"} isOpen={orderVisibility} toggle={closeOrderModal}>
            <ModalHeader toggle={closeOrderModal} className={"bg-light text-white"}>
                {page === 'message' ? 'New Order' : 'Edit Order'}
            </ModalHeader>
            <ModalBody>
                <AvForm onSubmit={handleSubmit} id={"form"}>
                    <table className="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Mahsulot nomi</th>
                            <th>3 D o'lcham</th>
                            <th>Material</th>
                            <th>Knife</th>
                            <th>Jarayon</th>
                            <th>Turi</th>
                            <th>Miqdori</th>
                            <th>Narxi</th>
                            <th>O'chirish</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            inputRow.map((item, index) => {
                                return (
                                    <tr key={item}>
                                        <td>{index + 1}</td>
                                        <td><AvInput name={"productName/" + index} type="text"
                                                     className={"form-control mt-2"}
                                                     value={item.productName} required/>
                                        </td>
                                        <td><AvInput name={"threeDSize/" + index} type="text"
                                                     className={"form-control mt-2"}
                                                     value={item.threeDSize}/>
                                        </td>
                                        <td><AvInput name={"material/" + index} type="text"
                                                     className={"form-control mt-2"}
                                                     value={item.material}/>
                                        </td>
                                        <td><AvInput name={"knife/" + index} type="text"
                                                     className={"form-control mt-2"}
                                                     value={item.knife}/>
                                        </td>
                                        <td><AvInput name={"process/" + index} type="text"
                                                     className={"form-control mt-2"}
                                                     value={item.process}/>
                                        </td>
                                        <td><AvInput name={"type/" + index} type="text"
                                                     className={"form-control mt-2"}
                                                     value={item.type}/>
                                        </td>
                                        <td><AvInput name={"price/" + index} type="number"
                                                     className={"form-control mt-2"}
                                                     value={item.price} required/>
                                        </td>
                                        <td><AvInput name={"amount/" + index} type="number"
                                                     className={"form-control mt-2"}
                                                     value={item.amount} required/>
                                        </td>
                                        <td>
                                            <button onClick={() => deleteRow(index)}
                                                    className={"btn btn-outline-danger"}> -
                                            </button>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                        </tbody>
                    </table>
                    <button type={"button"} onClick={addProduct} className={"btn btn-success mt-2"}>Mahsulot qo'shish
                    </button>
                </AvForm>
            </ModalBody>
            <ModalFooter>
                <button form={"form"} className={"btn btn-outline-success"}>
                    {page === 'message' ? 'Buyurtma yaratish' : 'Buyurmani saqlash'}
                </button>
                <button onClick={closeOrderModal} className={"btn btn-outline-danger"}>Bekor qilish</button>
            </ModalFooter>
        </Modal>
    )
};

export default OrderModal;
