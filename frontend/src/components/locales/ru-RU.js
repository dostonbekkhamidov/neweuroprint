
export default {
  // Header section translate start
  header_link1: "О нас",
  header_link2: "Услуги",
  header_link3: "Портфолио",
  header_link4: "Клиенты",
  header_link5: "Команда",
  header_link6: "Контакт",
  header_link7: "Aдмин",
  header_item1: "O`zbekcha",
  header_item2: "Русский",
  header_text:"Хотите увеличить продажи своего продукта? " +
    "Тогда обратите внимание, на современность вашей упаковки. ",
  header_text_bold: "Закажите у Нас современную упаковку!",
  header_btn: "Заказать",
  // Header section translate start



  // Section 1 translate start
  section1_Title: "О компании.",
  section1_Text: "Europrint - надежный партнер для развития и успеха вашего бизнеса.\n" +
    "Наша компания очень рада предложить Вам свои услуги с использованием новейших " +
    "технологий и с командой высококвалифицированных специалистов. \n" +
    "Наша компания готова делать все, чтобы создать имидж вашего " +
    "продукта и доставить его, тем самым, увеличивая ваши продажи " +
    "и предоставляя вам возможность оказаться на рынке.\n",
  section1_Sub_Text1: "Творческий подход",
  section1_Sub_Text2: "Современные технологии",
  section1_Sub_Text3: "Современный подход к предоставлению услуг",
  section1_Sub_Text4: "Совместная разработка",
  // Section 1 translate end


  // Section 2 translate start
  section2_Main_Title: "Почему мы?",
  section2_Sub_Title1: "Эффективность",
  section2_Text1: "Эффективность - это современная технология автоматизации.",
  section2_Sub_Title2: "Качество",
  section2_Text2: "Заказы систематически проверяются в нашей лаборатории, " +
    "как по внешним, так и по внутренним качественным показателям.",
  section2_Sub_Title3: "Удобная цена",
  section2_Text3: "Является наиболее важным фактором в поддержании" +
    " партнерства, в соответствии с ценностями нашей компании.",
  section2_Sub_Title4: "Современное оборудование",
  section2_Text4: "Компания обеспечена высокотехнологичными " +
    "оборудованиями ведущих производителей.",
  section2_Sub_Title5: "Дизайн",
  section2_Text5: "Это индивидуальный подход к созданию образа продукта," +
    " выгодный как для продавца, так и для покупателя одновременно.",
  // Section 2 translate end


  // Section 3 translate start
  section3_Title: "Наши услуги",
  section3_Sub_Title1: "Гофра коробка",
  section3_Sub_Title2: "Этикетка",
  section3_Sub_Title3: "Флаер",
  section3_Sub_Title4: "Блокнот",
  section3_Sub_Title5: "Листовка",
  section3_Sub_Title6: "Буклет",
  section3_Sub_Title7: "Календарь",
  section3_Sub_Title8: "Каталог",
  section3_Sub_Title9: "Визитка",
  section3_Sub_Title10: "Журнал",
  section3_Sub_Title11: "Бумажный стаканчик",
  section3_Sub_Title12: "Бумажные крафт пакеты",
  section3_Text: "Узнать больше",
  section3_Btn: "Заказать",
  // Section 3 translate end


  // Section 4 translate start
  section4_Title: "Портфолио",
  section4_Text: "Наша Компания поставила перед собой грандиозные планы. " +
    "Каждое достижение на этом пути очень важно для нас. " +
    "Поэтому мы хотим поделиться с Вами.",
  // Section 4 translate end


  // Section 5 translate start
  section5_Title: "Наши достижения",
  section5_Text1: "Количество наших клиентов",
  section5_Text2: "Количество выполненных заказов",
  section5_Text3: "Наши специалисты",
  // Section 5 translate end


  // Section 6 translate start
  section6_Title: "Наши клиенты",
  // Section 6 translate end


  // Section 7 translate start
  section7_Title: "Наша команда",
  section7_User_Name1: "Brandon Richards",
  section7_User_Profession1: "Фотограф, главный дизайнер",
  section7_User_Name2: "Beth Mckinney",
  section7_User_Profession2: "Начальник отдела обслуживания клиентов",
  section7_User_Name3: "Dianne Fisher",
  section7_User_Profession3: "Менеджер по обслуживанию клиентов",
  section7_User_Name4: "Dianne Fisher",
  section7_User_Profession4: "Менеджер по обслуживанию клиентов",
  section7_User_Name5: "Brandon Richards",
  section7_User_Profession5: "Фотограф, главный дизайнер",
  section7_User_Name6: "Beth Mckinney",
  section7_User_Profession6: "Начальник отдела обслуживания клиентов",
  // Section 7 translate end


  // Footer translate start
  section8_Title1: "Компания",
  section8_Title1_Link1: "О нас",
  section8_Title1_Link2: "Продукты",
  section8_Title1_Link3: "Услуги",
  section8_Title1_Link4: "Наши клиенты",
  section8_Title1_Link5: "Наша Команда",
  section8_Title2: "Услуги ",
  section8_Title2_Link1: "Шелкография",
  section8_Title2_Link2: "Офсетная печать",
  section8_Title2_Link3: "Высечка",
  section8_Title2_Link4: "УФ лакирования",
  section8_Title2_Link5: "Тиснение",
  section8_Title2_Link6: "Ламинация",
  section8_Title2_Link7: "Кашировка бумаги",
  section8_Title2_Link8: "Вырубка",
  section8_Title3: "Для клиентов",
  section8_Title3_Link1: "Руководство",
  section8_Title3_Link2: "История",
  section8_Title3_Link3: "Новости",
  section8_Title3_Link4: "Требование к макетам",
  section8_Title3_Link5: "Контакты",
  section8_Title4: "Контакт",
  language_item1: "O`zbekcha",
  language_item2: "Русский",
  footer_linkText_Info: "© 2005 – 2019  " +
    "типография  europrint.uz |" +
    "  Personal Development Process",
  // Footer translate end

}
