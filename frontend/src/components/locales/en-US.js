
export default {
  // Header section translate start
  header_link1: "Biz haqimizda",
  header_link2: "Xizmatlar",
  header_link3: "Portfolio",
  header_link4: "Mijozlar",
  header_link5: "Jamoa",
  header_link6: "Kontakt",
  header_link7: "Admin",
  header_item1: "O`zbekcha",
  header_item2: "Русский",
  header_text: "Mahsulotlaringiz savdosi oshishini xohlaysizmi? " +
    "Unday bo'lsa, mahsulotingiz qadog'ining zamonaviyligiga e'tibor bering.",
  header_text_bold: "Zamonaviy qadoq uchun buyurtma bering!",
  header_btn: "Buyurtma berish",
  // Header section translate start


  // Section 1 translate start
  section1_Title: "Kompaniya haqida",
  section1_Text: "Europrint kompaniyasi - bu biznesingiz yanada rivojlanishi va muvaffaqiyatga" +
    " erishishidagi ishonchli hamkor Kompaniyamiz buning uchun eng zamonaviy texnopark va yuqori " +
    "tajribali jamoa bilan o’z xizmatlarini taklif qilishdan benihoya hursand.\n" +
    "Kompaniyamiz Sizning mahsulotingizni zamonaviy imidjini yaratish va yetkazib berish," +
    " bu orqali savdoingizni oshirish, hamda bozorda o’z o’rningizni topishingizda bor " +
    "imkoniyatlarini berishga tayyor.\n",
  section1_Sub_Text1: "Kreativ yondashuv",
  section1_Sub_Text2: "Zamonaviy texnologiya",
  section1_Sub_Text3: "Xizmat ko’rsatishdagi zamonaviy yondashuv",
  section1_Sub_Text4: "Birgalikda rivojlanish",
  // Section 1 translate end


  // Section 2 translate start
  section2_Main_Title: "Nega biz?",
  section2_Sub_Title1: "Samaradorlik",
  section2_Text1: "Buyurtmalarni bajarishda avtomatlashtirilgan zamonaviy texnologiya.",
  section2_Sub_Title2: "Sifat",
  section2_Text2: "Buyurtmalar kompaniyaning tashqi va ichki sifat ko’rsatkichlarida" +
    " tizimli ravishda o’z laboratoriyamizda tekshirib boriladi.",
  section2_Sub_Title3: "Qulay narx",
  section2_Text3: "Kompaniyamiz qadriyatlariga ko’ra," +
    " hamkorlikda ikkala taraf uchun ham manfaatlik ish olib" +
    " borish, bu hamkorlikni bardavomligi ta’minlavchi eng katta omilidir.",
  section2_Sub_Title4: "Zamonaviy uskunalar",
  section2_Text4: "Ishlab chiqarish sohasining yetakchi brendlari tomonidan" +
    " ishlab chiqarilgan uskunalar bilan ta’minlangan.",
  section2_Sub_Title5: "Dizayn",
  section2_Text5: "Mahsulotlar uchun imidj yaratishda, avvalo," +
    " ishlab chiqaruvchi va xaridorlarga" +
    " qulay bo’lishi uchun individual yondashuv bilan ish ko’rish",
  // Section 2 translate end


  // Section 3 translate start
  section3_Title: "Bizning xizmatlar",
  section3_Sub_Title1: "Gofra karobka",
  section3_Sub_Title2: "Etiketka",
  section3_Sub_Title3: "Flaer",
  section3_Sub_Title4: "Bloknot",
  section3_Sub_Title5: "Listovka",
  section3_Sub_Title6: "Buklet",
  section3_Sub_Title7: "Kalendar",
  section3_Sub_Title8: "Katalog",
  section3_Sub_Title9: "Visitka",
  section3_Sub_Title10: "Jurnal",
  section3_Sub_Title11: "Qog'oz stakan",
  section3_Sub_Title12: "Kraft qog'oz paketlar",
  section3_Text: "Batafsil",
  section3_Btn: "Buyurtma berish",
  // Section 3 translate end


  // Section 4 translate start
  section4_Title: "Portfolio",
  section4_Text: "Kompaniyamiz o'z oldiga katta rejalarni qo'ygan. " +
    "Bu yo`lda har bir erishilgan natijalar biz uchun qadrlikdir shu " +
    "boisdan sizlar bilan bo'lishishni hohladik.",
  // Section 4 translate end


  // Section 5 translate start
  section5_Title: "Bizning yutuqlarimiz",
  section5_Text1: "Bizning mijozlarimiz soni",
  section5_Text2: "Bajarilgan buyurtmalar soni",
  section5_Text3: "Bizning mutaxasislarimiz",
  // Section 5 translate end


  // Section 6 translate start
  section6_Title: "Bizning mijozlar",
  // Section 6 translate end


  // Section 7 translate start
  section7_Title: "Bizning jamoa",
  section7_User_Name1: "Brandon Richards",
  section7_User_Profession1: "Fotograf, Bosh dizayner",
  section7_User_Name2: "Beth Mckinney",
  section7_User_Profession2: "Mijozlar bilan ishlash bo’lim boshlig’i",
  section7_User_Name3: "Dianne Fisher",
  section7_User_Profession3: "Mijozlar bilan ishlash meneger",
  section7_User_Name4: "Dianne Fisher",
  section7_User_Profession4: "Mijozlar bilan ishlash meneger",
  section7_User_Name5: "Brandon Richards",
  section7_User_Profession5: "Fotograf, Bosh dizayner",
  section7_User_Name6: "Beth Mckinney",
  section7_User_Profession6: "Mijozlar bilan ishlash bo’lim boshlig’i",
  // Section 7 translate end

  // Footer translate start
  section8_Title1: "Kompaniya",
  section8_Title1_Link1: "Biz haqimizda",
  section8_Title1_Link2: "Mahsulotlar",
  section8_Title1_Link3: "Xizmatlar",
  section8_Title1_Link4: "Bizning Mijozlar",
  section8_Title1_Link5: "Bizning Jamoa",
  section8_Title2: "Xizmatlar",
  section8_Title2_Link1: "Ipakli bosma",
  section8_Title2_Link2: "Ofset bosma",
  section8_Title2_Link3: "Kesish",
  section8_Title2_Link4: "Ultra-binafsha rang",
  section8_Title2_Link5: "Bo'rttirish",
  section8_Title2_Link6: "Laminatsiya",
  section8_Title2_Link7: "Qog'oz lashirovka",
  section8_Title2_Link8: "O'yib olish",
  section8_Title3: "Mijozlar uchun",
  section8_Title3_Link1: "Rahbariyat",
  section8_Title3_Link2: "Tarix",
  section8_Title3_Link3: "Yangiliklar",
  section8_Title3_Link4: "Maketlar uchun talab",
  section8_Title3_Link5: "Aloqa",
  section8_Title4: "Aloqa",
  language_item1: "O`zbekcha",
  language_item2: "Русский",
  footer_linkText_Info: "© 2005 - 2019 europrint.uz tipografiyasi | Personal\n" +
    "Development Process",
  // Footer translate end
}
