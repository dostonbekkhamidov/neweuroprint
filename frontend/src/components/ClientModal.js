import React from "react";
import {Modal, ModalHeader, ModalBody, ModalFooter, Button, Label} from "reactstrap";
import {AvForm, AvGroup, AvInput} from "availity-reactstrap-validation";
import MaskedInput from 'react-text-mask'


export const ClientModal = ({
                                modalVisible,
                                closeClientModal,
                                saveClient,
                                modalType,
                                currentClient
                            }) => {

    return (
        <Modal isOpen={modalVisible} toggle={closeClientModal}>
            <ModalHeader toggle={closeClientModal}> Mijoz {modalType === "edit"? "o`zgartirish" :"qo`shishi"} </ModalHeader>

            <AvForm onValidSubmit={saveClient}>
                <ModalBody>
                    <AvGroup>
                        <AvInput value={currentClient.fullName} name="fullName" id="fullName" required/>
                        <Label for="fullName">Ismi, sharifi...</Label>
                    </AvGroup>
                    <AvGroup>
                        <AvInput value={currentClient.phoneNumber} name="phoneNumber" id="phoneNumber" required/>
                        <Label for="phoneNumber">Telefon raqamini kiriting</Label>
                    </AvGroup>
                    <AvGroup>
                        <MaskedInput className={"form-control"}
                            mask={['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]}
                        />
                    </AvGroup>
                </ModalBody>
                <ModalFooter>
                    <Button type="submit">Saqlash</Button>
                    <Button onClick={closeClientModal}>Bekor qilish</Button>
                </ModalFooter>
            </AvForm>
        </Modal>
    )

};

export default ClientModal;