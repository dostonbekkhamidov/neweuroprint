import React, {Component} from 'react';
import axios from 'axios';
import {PATH_PREFIX} from "../../utils";
import Pagination from "react-js-pagination";
import PaymentModal from 'src/components/PaymentModal';
import ClientModal from "src/components/ClientModal";
import {toast} from 'react-toastify';
import '../../index.scss';
import 'react-toastify/dist/ReactToastify.css';
import OrderModal from "src/components/OrderModal";
import {library} from '@fortawesome/fontawesome-svg-core';
import {
    faDollarSign,
    faEnvelope,
    faFileInvoice, faInfoCircle,
    faReceipt,
    faUserPlus,
    faUserTimes
} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faTrashAlt} from "@fortawesome/free-regular-svg-icons";
import {Popconfirm} from "antd";

class Index extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            activePage: 1,
            totalElements: 0,
            payModalVisible: false,
            currentClient: '',
            clientModalVisible: false,
            modalType: "create",
            orderVisibility: false,
            inputRow: [0],
            clientId: "",
            user: '',
        }
    };

    componentDidMount() {
        toast.configure({
            autoClose: 5000,
            draggable: false,
        });
        this.getUser();
        this.getClients();
    };

    //get current user
    getUser = () => {
        axios({
            url: PATH_PREFIX + '/api/user/me',
            method: 'get',
            headers: {
                'Authorization': localStorage.getItem('token')
            }
        }).then((response) => {
            if (response.data.success) {
                this.setState({
                    user: response.data.object
                })
            }
        })
    };

    //clint service begin
    // get all clients with current balances
    getClients = () => {
        axios({
            url: PATH_PREFIX + '/api/client',
            method: 'get',
            params: {
                search: '',
                page: 1,
                size: 10
            },
            headers: {
                'Authorization': localStorage.getItem('token'),
            }
        }).then((response) => {
            this.setState({
                data: response.data.object,
                totalElements: response.data.totalElements
            })
        });
    };
    openClientModal = () => {
        this.setState({
            clientModalVisible: true
        })
    };
    closeClientModal = () => {
        this.setState({
            clientModalVisible: false,
            currentClient: "",
            modalType: "create"
        })
    };
    openClientEditModal = (item) => {
        this.setState({
            clientModalVisible: true,
            currentClient: item,
            modalType: "edit"

        });
    };
    saveClient = (e, result) => {
        if (this.state.modalType === "create") {
            axios({
                url: PATH_PREFIX + '/api/client',
                method: 'post',
                data: result,
                headers: {
                    'Authorization': localStorage.getItem('token'),
                }
            }).then((response) => {
                if (response.data.success) {
                    this.setState({
                        clientModalVisible: false
                    });
                    this.getClients();
                    toast.success("Client successfully created")
                } else {
                    this.setState({
                        clientModalVisible: false
                    });
                    toast.error("something went wrong");
                }
            });
        } else {
            axios({
                url: PATH_PREFIX + '/api/client/' + this.state.currentClient.uuid,
                method: 'patch',
                data: result,
                headers: {
                    'Authorization': localStorage.getItem('token'),
                }
            }).then((response) => {
                if (response.data.success) {
                    this.setState({
                        clientModalVisible: false
                    });
                    this.getClients();
                }
            });
        }
    };
    deleteClient = (id) => {
        axios({
            url: PATH_PREFIX + "/api/client/" + id,
            method: "delete",
            headers: {
                'Authorization': localStorage.getItem('token'),
            }
        }).then((responseDelete) => {
            if (responseDelete.data.success) {
                this.getClients();
                toast.success("client successfully deleted")
            } else {
                toast.error("something went wrong")
            }
        })
    };
    //clint service end

    //orderService begin
    addProduct = () => {
        const {inputRow} = this.state;
        if (inputRow.length === 1) {
            inputRow.push(1)
        } else {
            let newRow = inputRow[inputRow.length - 1] + 1;
            inputRow.push(newRow);
        }
        this.setState({
            inputRow
        })
    };
    closeOrderModal = () => {
        this.setState({
            orderVisibility: false,
            inputRow: [0]
        })
    };
    openOderModal = (item) => {
        this.setState({
            orderVisibility: true,
            clientId: item.uuid
        });
    };
    deleteRow = (index) => {
        const {inputRow} = this.state;
        inputRow.splice(index, 1);
        this.setState({
            inputRow
        })
    };
    saveOrder = (products) => {
        const {clientId} = this.state;
        axios({
            url: PATH_PREFIX + '/api/order',
            method: 'post',
            data: {
                products,
                isBot: false,
                clientId: clientId,
                messageId: null
            },
            headers: {
                'Authorization': localStorage.getItem('token')
            }
        }).then((response) => {
            if (response.data.success) {
                this.setState({
                    orderVisibility: false,
                    modalVisibility: false,
                    currentMessage: ""
                })
            }
        });

    };

    //orderService end

    render() {
        const {user, data, activePage, totalElements, payModalVisible, currentClient} = this.state;

        const handlePageChange = (pageNumber) => {
            this.setState({
                activePage: pageNumber
            });
            axios({
                url: PATH_PREFIX + '/api/client',
                method: 'get',
                params: {
                    search: '',
                    page: pageNumber
                },
                headers: {
                    'Authorization': localStorage.getItem('token')
                }
            }).then(function (response) {
                this.setState({
                    data: response.data.object,
                    totalElements: response.data.totalElements
                })
            }.bind(this));
        };

        const openPaymentModal = (uuid) => {
            this.setState({
                payModalVisible: true,
                currentClient: uuid
            })
        };
        const closePaymentModal = (uuid) => {
            this.setState({
                payModalVisible: false,
                currentClient: ''
            })
        };
        const savePayment = (event, errors, values) => {
            this.setState({
                payModalVisible: false,
            });
            if (errors.length === 0) {
                axios({
                    url: PATH_PREFIX + '/api/payment',
                    method: 'post',
                    data: {...values, clientId: currentClient},
                    headers: {
                        'Authorization': localStorage.getItem('token'),
                    }
                }).then((response) => {
                    if (response.data.success) {
                        axios({
                            url: PATH_PREFIX + '/api/client',
                            method: 'get',
                            params: {
                                search: '',
                                page: 1,
                                size: 2
                            },
                            headers: {
                                'Authorization': localStorage.getItem('token'),
                            }
                        }).then((response) => {
                            this.setState({
                                data: response.data.object,
                                totalElements: response.data.totalElements
                            })
                        });
                    }
                });
            }
        };

        library.add(faEnvelope, faDollarSign, faFileInvoice, faUserTimes, faUserPlus, faInfoCircle, faTrashAlt,
            faReceipt);

        return (

            <div className={"container"}>
                <PaymentModal payModalVisible={payModalVisible}
                              currentClient={currentClient}
                              clients={[]}
                              closePaymentModal={closePaymentModal}
                              savePayment={savePayment}
                />
                <ClientModal modalVisible={this.state.clientModalVisible}
                             closeClientModal={this.closeClientModal}
                             saveClient={this.saveClient}
                             modalType={this.state.modalType}
                             currentClient={this.state.currentClient}
                />
                <OrderModal
                    inputRow={this.state.inputRow}
                    addProduct={this.addProduct}
                    closeOrderModal={this.closeOrderModal}
                    deleteRow={this.deleteRow}
                    orderVisibility={this.state.orderVisibility}
                    saveOrder={this.saveOrder}
                />
                <div className="row mt-5">
                    <div className="col-md text-center">
                        <table className="table my-3">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Ismi, sharifi</th>
                                <th>Telefon raqami</th>
                                <th>Hisobi</th>
                                <th>
                                    <div className={"text-left"}>
                                        <span className={"mr-4 pr-3 "}>Amallar</span>
                                        <button onClick={this.openClientModal} className={"btn btn-outline-dark ml-5"}>
                                            <FontAwesomeIcon icon={"user-plus"}/></button>
                                    </div>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                data.map((item, index) => {
                                    return (
                                        <tr key={index}>
                                            <td>{index + 1}</td>
                                            <td>{item.fullName !== null ? item.fullName : 'N/A'}</td>
                                            <td>{item.phoneNumber}</td>
                                            <td>{item.clientBalance}</td>
                                            <td className={"float-left"}>
                                                <button onClick={() => this.openClientEditModal(item)}
                                                        className={"btn btn-dark"}><FontAwesomeIcon
                                                    icon={"user-edit"}/>
                                                </button>
                                                {' '}
                                                <button onClick={() => this.openOderModal(item)}
                                                        className={"btn btn-dark"}>+ <FontAwesomeIcon
                                                    icon={"file-invoice"}/></button>
                                                {' '}
                                                <button onClick={() => openPaymentModal(item.uuid)}
                                                        className={"btn btn-dark"}>+ <FontAwesomeIcon
                                                    icon={"dollar-sign"}/>
                                                </button>
                                                {' '}
                                                {
                                                    user && user.roles.filter(item => item.roleName === ('ROLE_DIRECTOR' || 'ROLE_ADMIN')).length === 0 ? '' :
                                                        <Popconfirm className={"delete-portfolio"}
                                                                    onConfirm={() => this.deleteClient(item.uuid)}
                                                                    title="Ishonchingiz komilmi?"
                                                                    okText="Ha" cancelText="Yo`q">
                                                            <button className={"btn btn-danger"}>
                                                                <FontAwesomeIcon icon={"user-times"}/>
                                                            </button>
                                                        </Popconfirm>
                                                }
                                            </td>
                                        </tr>
                                    )
                                })
                            }
                            </tbody>
                        </table>
                        <Pagination
                            activePage={activePage}
                            itemsCountPerPage={10}
                            totalItemsCount={totalElements}
                            nextPageText={">"}
                            prevPageText={"<"}
                            onChange={handlePageChange}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

export default Index;