import React, {Component} from 'react';
import axios from "axios";
import {PATH_PREFIX} from "src/utils";
import {Col, Row} from "reactstrap";

class Index extends Component {
    render() {
        const handleLogin=(event)=>{
            event.preventDefault(); // prevent from F5
            const username = event.target[0].value;
            const password = event.target[1].value;

            return axios({
                url: PATH_PREFIX + '/api/auth',
                method: 'post',
                data: {
                    username,
                    password,
                },
                // headers: {
                //     'Authorization':localStorage.getItem('token')
                // }
            }).then(function (response) {
                localStorage.setItem('token',response.data.tokenType + " " + response.data.accessToken);
                window.location.reload(true);
            })
        };

        return (
            <div>
                {/*<div className="container">*/}
                {/*    <div className="row mt-5">*/}
                {/*        <div className="col-md-4 offset-4">*/}
                {/*            <div className="card">*/}
                {/*                <div className="card-header bg-primary text-white">*/}
                {/*                    <h3>Login</h3>*/}
                {/*                </div>*/}
                {/*                <div className="card-body ">*/}
                {/*                    <form onSubmit={handleLogin}>*/}
                {/*                        <input type="text" className={"form-control mt-2"} placeholder={"username"}/>*/}
                {/*                        <input type="password" className={"form-control mt-2"} placeholder={"password"}/>*/}
                {/*                        <button className={"btn btn-success mt-2"} type={"submit"}>Login</button>*/}
                {/*                    </form>*/}
                {/*                </div>*/}
                {/*            </div>*/}
                {/*        </div>*/}
                {/*    </div>*/}
                {/*</div>*/}
                <div id="login">
                    <form>
                        <Row>
                            <Col span='24'>
                                <img src="/assets/images/logo.png" width={145} height={55} className='pl-3 mt-4 ml-3' alt="wrk"/>
                            </Col>
                        </Row>
                        <Row className='mt-5 pt-4'>
                            <Col span={8} className=' offset-2 text-center mt-5' >
                                <Row style={{width: "426px"}}>
                                    <h4 className=' text-center mb-5 ml-5 pl-4' >Tizimga kirish</h4>
                                    <div className="row pl-5 mb-3 text-left">
                                        {/*<div className="col-md-1 p-0 userr">*/}
                                        {/*    <span id="nma"><Icon type="user" /></span>*/}
                                        {/*</div>*/}
                                        {/*<div className="col-md-10 userrr pl-0">*/}
                                        {/*    <AvGroup>*/}
                                        {/*        <AvInput name={"username"}/>*/}
                                        {/*    </AvGroup>*/}
                                        {/*</div>*/}
                                        {/*<AvGroup>*/}
                                        {/*    <AvInput name={"password"}/>*/}
                                        {/*</AvGroup>*/}
                                        <form onSubmit={handleLogin}>
                                            <div className="input-group mb-3">
                                                <div className="input-group-prepend">
                                                    <span className="input-group-text" id="basic-addon1">@</span>
                                                </div>
                                                <input type="text" className="form-control" placeholder="username" aria-label="Username" aria-describedby="basic-addon1"/>
                                            </div>
                                            <div className="input-group mb-3">
                                                <div className="input-group-prepend">
                                                    <span className="input-group-text" id="basic-addon1">&#128274;</span>
                                                </div>
                                                <input type="password" className="form-control" placeholder="password" aria-label="Username" aria-describedby="basic-addon1"/>
                                            </div>
                                            <button className={"btn btn-success mt-2"} type={"submit"}>Login</button>
                                        </form>
                                    </div>

                                </Row>
                            </Col>
                            <Col span='10' className="ml-5 pl-5">
                                <img src="assets/images/loginimg.png"  className='img-fluid' alt="pie"/>
                            </Col>
                        </Row>
                    </form>
                </div>
            </div>
        );
    }
}

export default Index;