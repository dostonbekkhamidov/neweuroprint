import React, {Component} from 'react';
import axios from 'axios';
import {PATH_PREFIX} from "../../utils";
import {Col, Row} from "reactstrap";
import Select from "react-select";
import 'bootstrap/dist/css/bootstrap.css';

class Index extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            dataTable: [],
            page: 1,
            size: 10,
            value: '',
            totalElements: 0,
            productAmount: 0,
            orderPayment: 0,
            payment: 0,
            paymentBack: 0,
            currentItem: '',
            defaultValue: '',
        }
    }

    componentDidMount() {
        axios({
            url: PATH_PREFIX + '/api/reconciliation',
            method: 'get',
            params: {
                page: this.state.page,
                size: this.state.size,
                search: this.state.value,
            },
            headers: {
                'Authorization': localStorage.getItem('token')
            }
        }).then((response) => {
            if (response.data.success) {
                let result = response.data.object;
                let dataArray = [];
                if (response.data.object !== null) {
                    result.map(item =>
                        dataArray.push({
                            value: item.id,
                            label: item.fullName !== null ? item.fullName + ' ' + item.phoneNumber : item.phoneNumber
                        })
                    )
                }
                this.setState({
                    data: dataArray
                })
            }
        })
    }

    onChange = (event) => {
        this.setState({
            currentItem: event,
            defaultValue: event.label
        });
        axios({
            url: PATH_PREFIX + '/api/reconciliation/client/' + event.value,
            method: 'get',
            params: {
                page: this.state.page,
                size: this.state.size
            },
            headers: {
                'Authorization': localStorage.getItem('token')
            },
        }).then(response => {
            if (response.data.success) {
                this.setState({
                    dataTable: response.data.object,
                    totalElements: response.data.totalElements,
                    productAmount: response.data.productAmount,
                    orderPayment: response.data.orderPayment,
                    payment: response.data.payment,
                    paymentBack: response.data.paymentBack,
                });
            }
        });
    };

    render() {

        const {data, dataTable, productAmount, orderPayment, payment, paymentBack, currentItem, defaultValue} = this.state;

        return (
            <div className={"container mt-5"}>
                <div className="row">
                    <div className="col-md-4">
                        <Select
                            defaultValue={defaultValue}
                            onChange={this.onChange}
                            options={data}
                            autoFocus={true}
                        />
                    </div>
                </div>
                <Row style={{marginTop: 30}}>
                    <Col span={5} offset={1}>
                        <div className="message card border-bottom-1 mt-2 border-dark">
                            <div className="card-body text-center">
                                <p style={{fontSize: 30}}>{productAmount}</p>
                                <p>Soni</p>
                            </div>
                        </div>
                    </Col>
                    <Col span={5} offset={1}>
                        <div className="message card border-bottom-1 mt-2 border-dark">
                            <div className="card-body text-center">
                                <p style={{fontSize: 30}}>{orderPayment}</p>
                                <p>Summasi</p>
                            </div>
                        </div>
                    </Col>
                    <Col span={5} offset={1}>
                        <div className="message card border-bottom-1 mt-2 border-dark">
                            <div className="card-body text-center">
                                <p style={{fontSize: 30}}>{payment !== null ? payment : 0}</p>
                                <p>To'lov</p>
                            </div>
                        </div>
                    </Col>
                    <Col span={5} offset={1}>
                        <div className="message card border-bottom-1 mt-2 border-dark">
                            <div className="card-body text-center">
                                <p style={{fontSize: 30}}>{payment - orderPayment - paymentBack}</p>
                                <p>Saldo</p>
                            </div>
                        </div>
                    </Col>
                </Row>
                <Row style={{marginTop: 30}}>
                    <Col>
                        {
                            dataTable.length !== 0 ?
                                <table className="table table-hover">
                                    <thead className="thead-light">
                                    <tr>
                                        <th>Sana</th>
                                        <th>Jarayon</th>
                                        <th>Nomi</th>
                                        <th>Miqdori</th>
                                        <th>Narxi</th>
                                        <th>Jami</th>
                                        <th>To`lov</th>
                                    </tr>
                                    </thead>
                                    <tbody className={"tableScroll"}>
                                    {
                                        dataTable.map((item, index) => {
                                            return (
                                                <tr key={index}>
                                                    <td>{item.createdAt.substring(0, 16)}</td>
                                                    <td>{item.process}</td>
                                                    <td>{item.name}</td>
                                                    <td>{item.amount}</td>
                                                    <td>{item.price}</td>
                                                    <td>{item.amount * item.price !== 0 ? item.amount * item.price : null}</td>
                                                    <td>{item.payment}</td>
                                                </tr>
                                            )
                                        })
                                    }
                                    </tbody>
                                </table>
                                : ''
                        }
                    </Col>
                </Row>
            </div>
        );
    }
}

export default Index;