import React, {Component} from 'react';
import axios from 'axios';
import './dashboard.scss';
import {PATH_PREFIX} from "../../utils";
import {Button, Label, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import {AvForm, AvGroup, AvInput} from "availity-reactstrap-validation";
import {toast} from "react-toastify";
import 'font-awesome/css/font-awesome.min.css';
import {Route} from "react-router-dom";
import {query} from "../../service";
import {library} from '@fortawesome/fontawesome-svg-core';
import {
    faDollarSign,
    faEnvelope,
    faFileInvoice, faInfoCircle,
    faKey, faReceipt, faUserEdit,
    faUserPlus,
    faUserTimes
} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faTrashAlt} from "@fortawesome/free-regular-svg-icons";


class Index extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            ignoredDate: [],
            total: 0,
            accepted: 0,
            pricing: 0,
            designing: 0,
            consulting: 0,
            ignored: 0,
            isModalShow: false,
            historyModal: false,
            modalType: '',
            currentManager: ''
        }
    }

    componentDidMount() {
        this.getMessageByManager();
        toast.configure({
            autoClose: 5000,
            draggable: false,
        });
    }

    getMessageByManager = () => {
        axios({
            url: PATH_PREFIX + '/api/user/all',
            method: 'get',
            headers: {
                'Authorization': localStorage.getItem('token')
            }
        }).then((response) => {
            if (response.data.success){
                const lastArray = response.data.object[response.data.object.length - 1];
                response.data.object.splice(response.data.object.length - 1);
                this.setState({
                    data: response.data.object,
                    total: lastArray.total,
                    accepted: lastArray.accepted,
                    pricing: lastArray.pricing,
                    designing: lastArray.designing,
                    consulting: lastArray.consulting,
                    ignored: lastArray.ignored
                })
            }
        });
    };
    openModal = () => {
        this.setState({
            isModalShow: true,
            modalType: 'add',
        })
    };
    closeModal = () => {
        this.setState({
            isModalShow: false,
            currentManager: '',
        })
    };
    openEditModal = (id) => {
        axios({
            url: PATH_PREFIX + '/api/user/' + id,
            method: 'get',
            headers: {
                'Authorization': localStorage.getItem('token')
            }
        }).then((response) => {
            if (response.data.success) {
                this.setState({
                    isModalShow: true,
                    modalType: 'edit',
                    currentManager: response.data.object,
                })
            }
        });
    };
    saveManager = (e, result) => {
        if (this.state.modalType === 'add') {
            axios({
                url: PATH_PREFIX + "/api/user",
                method: "post",
                data: result,
                headers: {
                    'Authorization': localStorage.getItem('token')
                }
            }).then((response) => {
                if (response.data.success) {
                    this.setState({
                        isModalShow: false,
                    });
                    toast.success("Manager successfully created");
                    this.getMessageByManager();
                }
            });
        } else {
            axios({
                url: PATH_PREFIX + "/api/user/" + this.state.currentManager.id,
                method: "patch",
                data: result,
                headers: {
                    'Authorization': localStorage.getItem('token')
                }
            }).then((response) => {
                if (response.data.success) {
                    this.setState({
                        isModalShow: false,
                        currentManager: '',
                    });
                    toast.success("Manager successfully edited");
                    this.getMessageByManager();
                }
            });
        }
    };
    openHistoryModal = (id) => {
        axios({
            url: PATH_PREFIX + '/api/user/ignored/' + id,
            method: 'get',
            headers: {
                'Authorization': localStorage.getItem('token')
            }
        }).then(response => {
            if (response.data.success) {
                if (response.data.object.length > 0) {
                    this.setState({
                        ignoredDate: response.data.object,
                        historyModal: true,
                    })
                }
            }
        });
    };
    closeIgnoredModal = () => {
        this.setState({
            historyModal: false
        })
    };

    render() {

        const {
            data, total, accepted, pricing, designing, consulting, ignored, isModalShow, modalType,
            historyModal, ignoredDate
        } = this.state;
        library.add(faEnvelope, faDollarSign, faFileInvoice, faUserTimes, faUserPlus, faInfoCircle, faTrashAlt,
            faReceipt, faUserEdit);


        return (
            <div className="wrapper container">
                <Modal isOpen={isModalShow} toggle={this.closeModal}>
                    <ModalHeader toggle={this.closeModal}>
                        {modalType === 'add' ? 'Add manager' : 'Edit manager'}</ModalHeader>
                    <AvForm onValidSubmit={this.saveManager}>
                        <ModalBody>
                            <AvGroup>
                                <AvInput value={this.state.currentManager.firstName} name="firstName" id="firstName"
                                         required/>
                                <Label for="firstName">Ismi</Label>
                            </AvGroup>
                            <AvGroup>
                                <AvInput value={this.state.currentManager.lastName} name="lastName" id="lastName"
                                         required/>
                                <Label for="lastName">Sharifi</Label>
                            </AvGroup>
                            <AvGroup>
                                <AvInput value={this.state.currentManager.phoneNumber} name="phoneNumber"
                                         id="phoneNumber" required/>
                                <Label for="phoneNumber">Telefon raqami</Label>
                            </AvGroup>
                            <AvGroup>
                                <AvInput value={this.state.currentManager.passportSerial} name="passportSerial"
                                         id="passportSerial" required/>
                                <Label for="passportSerial">Pasport seriasi</Label>
                            </AvGroup>
                            <AvGroup>
                                <AvInput value={this.state.currentManager.passportNumber} name="passportNumber"
                                         id="passportNumber" required/>
                                <Label for="passportNumber">Pasport raqami</Label>
                            </AvGroup>
                            <AvGroup>
                                <AvInput value={this.state.currentManager.username} name="username" id="username"
                                         required/>
                                <Label for="username">Foydalanuvchi nomi</Label>
                            </AvGroup>
                            <AvGroup>
                                <AvInput name="password" id="password" required/>
                                <Label for="password">Parol</Label>
                            </AvGroup>
                        </ModalBody>
                        <ModalFooter>
                            <Button type="submit">{this.state.modalType === "edit" ? "Save" : "Add"}</Button>
                            <Button onClick={this.closeModal}>Bekor qilish</Button>
                        </ModalFooter>
                    </AvForm>
                </Modal>
                <Modal isOpen={historyModal} toggle={this.closeIgnoredModal} className={"orderModal"}>
                    <ModalHeader toggle={this.closeIgnoredModal}>
                        <h3>Bekor qilingan xabarlar tarixi</h3>
                    </ModalHeader>
                    <ModalBody>
                        <div className="row">
                            <div className="col-md">
                                <table className="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Sana</th>
                                        <th>Xabar</th>
                                        <th>Sabab</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {
                                        ignoredDate.map((item, index) => {
                                            return (
                                                <tr>
                                                    <td>{index + 1}</td>
                                                    <td>{item.createdAt.substring(0, 10) + ' ' +
                                                    item.createdAt.substring(11, 16)}</td>
                                                    <td>{item.text}</td>
                                                    <td>{item.reason}</td>
                                                </tr>
                                            )
                                        })
                                    }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        <button onClick={this.closeIgnoredModal} className={"btn btn-outline-secondary"}>Yopish</button>
                    </ModalFooter>
                </Modal>
                <div className="row mt-4">
                    <div className="col-md-10">
                        <h2>Xabarlar statistikasi</h2>
                    </div>
                    <div className="col-md-2 text-right mt-3">
                        <Button onClick={this.openModal}><FontAwesomeIcon icon={"user-plus"}/></Button>
                    </div>
                </div>
                <div className="row mt-4">
                    <div className="col-md col-sm-6 col-xs-12">
                        <a className="dashboard-stat blue" href="#">
                            <div className="visual">
                                <i className="fa fa-bar-chart-o"/>
                            </div>
                            <div className="details">
                                <div className="number">
                                    <span>{total}</span>
                                </div>
                                <div className="desc">Umumiy</div>
                            </div>
                        </a>
                    </div>
                    <div className="col-md col-sm-6 col-xs-12">
                        <a className="dashboard-stat green" href="#">
                            <div className="visual">
                                <i className="fa fa-bar-chart-o"/>
                            </div>
                            <div className="details">
                                <div className="number">
                                    <span>{accepted}</span>
                                </div>
                                <div className="desc">Qabul qilingan</div>
                            </div>
                        </a>
                    </div>
                    <div className="col-md col-sm-6 col-xs-12">
                        <a className="dashboard-stat hoki" href="#">
                            <div className="visual">
                                <i className="fa fa-credit-card"/>
                            </div>
                            <div className="details">
                                <div className="number">
                                    <span>{pricing}</span>
                                </div>
                                <div className="desc">Narxlash</div>
                            </div>
                        </a>
                    </div>
                    <div className="col-md col-sm-6 col-xs-12">
                        <a className="dashboard-stat purple" href="#">
                            <div className="visual">
                                <i className="fa fa-comments"/>
                            </div>
                            <div className="details">
                                <div className="number">
                                    <span>{designing}</span>
                                </div>
                                <div className="desc">Desaynlash</div>
                            </div>
                        </a>
                    </div>
                    <div className="col-md col-sm-6 col-xs-12">
                        <a className="dashboard-stat orange" href="#">
                            <div className="visual">
                                <i className="fa fa-comments"/>
                            </div>
                            <div className="details">
                                <div className="number">
                                    <span>{consulting}</span>
                                </div>
                                <div className="desc">Konsalting</div>
                            </div>
                        </a>
                    </div>
                    <div className="col-md col-sm-6 col-xs-12">
                        <a className="dashboard-stat red" href="#">
                            <div className="visual">
                                <i className="fa fa-usd"/>
                            </div>
                            <div className="details">
                                <div className="number">
                                    <span>{ignored}</span>
                                </div>
                                <div className="desc">Bekor qilingan</div>
                            </div>
                        </a>
                    </div>
                </div>
                <div className="row mt-4">
                    <div className="col-md">
                        <table className="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Menijer</th>
                                <th>Umumiy</th>
                                <th>Qabul qilingan</th>
                                <th>Narxlash</th>
                                <th>Dezayn</th>
                                <th>Konsalting</th>
                                <th>Bekor qilingan</th>
                                <th>Tanlov</th>
                                <th>Tarixi</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                data.map((item, index) => {
                                    return (
                                        <tr key={index}>
                                            <td>{index + 1}</td>
                                            <td>{item.fullName}</td>
                                            <td>{item.total}</td>
                                            <td>{item.accepted}</td>
                                            <td>{item.pricing}</td>
                                            <td>{item.designing}</td>
                                            <td>{item.consulting}</td>
                                            <td>{item.ignored}</td>
                                            <td>
                                                <Button onClick={() => this.openEditModal(item.userId)}>Edit</Button>
                                            </td>
                                            <td>
                                                <Button
                                                    onClick={() => this.openHistoryModal(item.userId)}>History</Button>
                                            </td>
                                        </tr>
                                    )
                                })
                            }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

export default Index;