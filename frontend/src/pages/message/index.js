import React, {Component} from 'react';
import axios from 'axios';
import {PATH_PREFIX} from "../../utils";
import {Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";
import {Tab, Tabs, TabList, TabPanel} from 'react-tabs';
import OrderModal from "../../components/OrderModal";
import SockJsClient from "react-stomp";
import 'react-tabs/style/react-tabs.css';
import {AvFeedback, AvForm, AvGroup, AvInput} from "availity-reactstrap-validation";
import { library } from '@fortawesome/fontawesome-svg-core';
import {faFileInvoice, faPaperPlane, faSave, faWindowClose} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


class Index extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            myData: [],
            myMessages: [],
            modalVisibility: false,
            orderVisibility: false,
            sendVisibility: false,
            ignoredModalVisibility: false,
            currentMessage: '',
            inputRow: [0],
            currentSize: 10,
            page: 'message',
            currentTab: 'ACTIVE',
            currentMessageId: '',
        }
    }

    componentDidMount() {
        this.activeMessage();
    }

    activeMessage = () => {
        axios({
            url: PATH_PREFIX + '/api/message',
            method: 'get',
            headers: {
                'Authorization': localStorage.getItem('token')
            }
        }).then(response => {
            if (response.data.currentMessage !== null && response.data.currentMessage.messageStatus !== "ACCEPTED") {
                this.setState({
                    currentMessage: response.data.currentMessage,
                    modalVisibility: true,
                });
            }
            this.setState({
                data: response.data.object
            });
        })
    };
    openModal = (item) => {
        if (!item.disabled) {                            // !item.disabled === true && item.disabled === false
            if (item.messageStatus === 'ACTIVE') {
                axios({
                    url: PATH_PREFIX + '/api/message/' + item.id,
                    method: 'patch',
                    headers: {
                        'Authorization': localStorage.getItem('token')
                    }
                })
            }
            this.setState({
                modalVisibility: true,
                currentMessage: item,
            });
            console.log(item, 333);

            if (item.order !== null) {
                axios({
                    url: PATH_PREFIX + '/api/product/me/' + item.order.id,
                    method: 'get',
                    headers: {
                        'Authorization': localStorage.getItem('token')
                    }
                }).then((response) => {
                    this.setState({
                        inputRow: response.data.object,
                    });
                    console.log(response.data.object, 444)
                });
            }
        }
    };
    closeModal = () => {
        const {currentMessage} = this.state;
        if (currentMessage.messageStatus === 'ACTIVE') {
            axios({
                url: PATH_PREFIX + '/api/message/' + currentMessage.id,
                method: 'patch',
                headers: {
                    'Authorization': localStorage.getItem('token')
                }
            });
        }
        this.setState({
            modalVisibility: false,
            currentMessage: '',
        });
    };
    openOderModal = () => {
        this.setState({
            modalVisibility: false,
            orderVisibility: true,
        })
    };
    closeOrderModal = () => {
        this.setState({
            modalVisibility: true,
            orderVisibility: false,
            inputRow: [0]
        })
    };
    addProduct = () => {
        const {inputRow} = this.state;
        if (inputRow.length === 1) {
            inputRow.push(1)
        } else {
            let newRow = inputRow[inputRow.length - 1] + 1;
            inputRow.push(newRow);
        }
        this.setState({
            inputRow
        })
    };
    deleteRow = (index) => {
        const {inputRow} = this.state;
        inputRow.splice(index, 1);
        this.setState({
            inputRow
        })
    };
    saveOrder = (products) => {
        const {currentMessage} = this.state;
        axios({
            url: PATH_PREFIX + '/api/order',
            method: 'post',
            data: {
                products,
                isBot: true,
                clientId: currentMessage.client.id,
                messageId: currentMessage.id
            },
            headers: {
                'Authorization': localStorage.getItem('token')
            }
        }).then((response) => {
            if (response.data.success) {
                axios({
                    url: PATH_PREFIX + '/api/message/me',
                    method: 'get',
                    params: {
                        status: currentMessage.messageStatus,
                        size: this.state.currentSize,
                    },
                    headers: {
                        'Authorization': localStorage.getItem("token")
                    }
                }).then(response => {
                    if (currentMessage.messageStatus === 'ACTIVE') {
                        this.setState({
                            data: response.data.object.content
                        })
                    } else {
                        this.setState({
                            myData: response.data.object.content
                        })
                    }
                });
                this.setState({
                    orderVisibility: false,
                    modalVisibility: false,
                    currentMessage: '',
                    inputRow: [0],
                })
            }
        });

    };
    handleSubmit = (event, errors, values) => {
        if (errors.length === 0) {
            if (values.length !== 0) {
                let param = values.reason;
                this.saveIgnoredMessage(param)
            }
        }
    };
    saveIgnoredMessage = (param) => {
        const {currentMessage} = this.state;
        axios({
            url: PATH_PREFIX + '/api/ignored',
            method: 'post',
            params: {
                reason: param,
                id: currentMessage.id,
                text: currentMessage.text,
            },
            headers: {
                'Authorization': localStorage.getItem('token')
            }
        }).then(response => {
            if (response.data.success) {
                axios({
                    url: PATH_PREFIX + '/api/message/status/' + currentMessage.id,
                    method: 'patch',
                    params: {
                        status: 'IGNORED',
                    },
                    headers: {
                        'Authorization': localStorage.getItem('token')
                    }
                }).then((response) => {
                    if (response.data.success) {
                        axios({
                            url: PATH_PREFIX + '/api/message/me',
                            method: 'get',
                            params: {
                                status: currentMessage.messageStatus,
                                size: this.state.currentSize,
                            },
                            headers: {
                                'Authorization': localStorage.getItem('token')
                            }
                        }).then((response) => {
                            if (currentMessage.messageStatus === 'ACTIVE') {
                                this.setState({
                                    data: response.data.object.content
                                })
                            } else {
                                this.setState({
                                    myData: response.data.object.content
                                })
                            }
                        });
                        this.setState({
                            modalVisibility: false,
                            ignoredModalVisibility: false,
                            currentMessage: '',
                        })
                    }
                })
            }
        })
    };
    closeIgnoredModal = () => {
        this.setState({
            ignoredModalVisibility: false,
            modalVisibility: true,
        })
    };
    changeMessageStatus = (status) => {
        const {currentMessage} = this.state;
        if (status === 'IGNORED') {
            this.setState({
                ignoredModalVisibility: true,
                modalVisibility: false,
                currentMessage: currentMessage,
            });
        } else {
            axios({
                url: PATH_PREFIX + '/api/message/status/' + currentMessage.id,
                method: 'patch',
                params: {
                    status: status
                },
                headers: {
                    'Authorization': localStorage.getItem('token')
                }
            }).then((response) => {
                if (response.data.success) {
                    axios({
                        url: PATH_PREFIX + '/api/message/me',
                        method: 'get',
                        params: {
                            status: currentMessage.messageStatus,
                            size: this.state.currentSize,
                        },
                        headers: {
                            'Authorization': localStorage.getItem('token')
                        }
                    }).then((response) => {
                        if (currentMessage.messageStatus === 'ACTIVE') {
                            this.setState({
                                data: response.data.object.content
                            })
                        } else {
                            this.setState({
                                myData: response.data.object.content
                            })
                        }
                    });
                    this.setState({
                        modalVisibility: false,
                        currentMessage: '',
                    })
                }
            })
        }
    };
    onMessageReceive = (message, topics) => {
        const {data} = this.state;
        if (topics === '/chat/changeStatus') {
            data.map(item => {
                if (item.id === message.id) {
                    item.disabled = message.disabled
                }
            });
            this.setState({
                data: data
            })
        } else {
            data.push(message);
            this.setState({
                data,
            })
        }
    };
    handleTab = (index) => {
        let param = '';
        switch (index) {
            case 0:
                this.activeMessage();
                return;
            case 1:
                param = 'PRICING';
                break;
            case 2:
                param = 'DESIGNING';
                break;
            case 3:
                param = 'CONSULTING';
                break;
            case 4:
                param = 'IGNORED';
                break;
        }
        this.setState({
            currentTab: param,
            currentSize: 10,
        });
        axios({
            url: PATH_PREFIX + '/api/message/me',
            method: 'get',
            params: {
                status: param,
                size: this.state.currentSize,
            },
            headers: {
                'Authorization': localStorage.getItem("token")
            }
        }).then(response => {
            this.setState({
                myData: response.data.object.content
            })
        })
    };
    handleScroll = () => {
        let myDiv = document.getElementById(this.state.currentTab);
        if (myDiv.offsetHeight + myDiv.scrollTop + 1 >= myDiv.scrollHeight) {
            this.setState({
                currentSize: this.state.currentSize + 10
            });
            axios({
                url: PATH_PREFIX + '/api/message/me',
                method: 'get',
                params: {
                    status: this.state.currentTab,
                    size: this.state.currentSize,
                },
                headers: {
                    'Authorization': localStorage.getItem("token")
                }
            }).then(response => {
                this.setState({
                    myData: response.data.object.content
                })
            })
        }
    };
    openSendModal = () => {
        this.setState({
            sendVisibility: true,
        })
    };
    closeSendModal = () => {
        this.setState({
            sendVisibility: false,
        })
    };
    handleSendForm = (event) => {
        event.preventDefault();
        if (event.target[1].files[0] === undefined) {
            if (event.target[0].value !== '') {
                axios({
                    url: PATH_PREFIX + '/api/chat',
                    method: 'post',
                    headers: {
                        'Authorization': localStorage.getItem('token')
                    },
                    data: {
                        text: event.target[0].value,
                        messageId: this.state.currentMessage.id,
                    }
                }).then(response=>{
                    if (response.data.success){
                        this.setState({
                            sendVisibility:false,
                        })
                    }
                })
            }
        } else {
            const a = new FormData();
            a.append('attachment', event.target[1].files[0]);
            a.append("messageId", this.state.currentMessage.id);
            axios({
                url: PATH_PREFIX + '/api/chat/file',
                method: 'post',
                data: a,
                headers: {
                    'Authorization': localStorage.getItem('token'),
                    "Content-Type": "multipart/form-data; boundary=----WebKitFormBoundaryqTqJIxvkWFYqvP5s",
                    "X-Requested-With": "XMLHttpRequest"
                }
            }).then(response=>{
                if (response.data.success){
                    this.setState({
                        sendVisibility:false,
                    })
                }
            })
        }
    };

    render() {
        const {
            data, myData, modalVisibility, currentMessage, orderVisibility, inputRow, sendVisibility,
            page, ignoredModalVisibility
        } = this.state;
        library.add(faPaperPlane,faFileInvoice,faSave,faWindowClose);

        return (
            <div className={"container"}>
                <SockJsClient url={PATH_PREFIX+'/socket'}
                              topics={["/chat/all", "/chat/changeStatus", "/chat/updateStatus"]}
                              onMessage={this.onMessageReceive} ref={(client) => {
                    this.clientRef = client;
                }}
                              onConnect={() => {
                                  this.setState({clientConnected: true})
                              }}
                              onDisconnect={() => {
                                  this.setState({clientConnected: false})
                              }}
                              debug={false}/>
                <div className="container">

                    <OrderModal
                        inputRow={inputRow}
                        addProduct={this.addProduct}
                        closeOrderModal={this.closeOrderModal}
                        deleteRow={this.deleteRow}
                        orderVisibility={orderVisibility}
                        saveOrder={this.saveOrder}
                        page={page}
                    />

                    <Modal isOpen={sendVisibility} toggle={this.closeSendModal}>
                        <ModalHeader toggle={this.closeSendModal}>
                            Xabar {currentMessage && currentMessage.client.phoneNumber} ga yuborilmoqda
                        </ModalHeader>
                        <ModalBody>
                            <form id={"sendForm"} onSubmit={this.handleSendForm}>
                                <input className={"form-control"} type="text-area" name={"text"}
                                       placeholder={"Type here!"}/>
                                <input className={"form-control"} type="file" name={"file"}/>
                            </form>
                        </ModalBody>
                        <ModalFooter>
                            <button className={"btn btn-outline-success"} type={'submit'} form={"sendForm"}><FontAwesomeIcon icon={"paper-plane"}/>
                            </button>
                            {' '}
                            <button className={"btn btn-outline-danger"} onClick={this.closeSendModal}><FontAwesomeIcon icon={"window-close"}/></button>
                        </ModalFooter>
                    </Modal>

                    <Modal isOpen={modalVisibility} toggle={this.closeModal}>
                        <ModalHeader toggle={this.closeModal}>
                            {currentMessage && currentMessage.client.phoneNumber}
                            {<button className={"float-right btn btn-outline-dark border- ml-5"}
                                     onClick={this.openSendModal}><FontAwesomeIcon icon={"paper-plane"}/></button>}
                            {' '}
                            {currentMessage && currentMessage.client.fullName}
                        </ModalHeader>
                        <ModalBody>
                            <div className="row">
                                <div className="col-md-8">{currentMessage.text}</div>
                                <div className="col-md-4">
                                    <button className={'btn btn-outline-dark'} onClick={this.openOderModal}>Buyurma qo`shish
                                    </button>
                                </div>
                            </div>
                        </ModalBody>
                        <ModalFooter>
                            <div className="row">
                                <button onClick={() => this.changeMessageStatus("PRICING")}
                                        className='btn btn-dark text-white mx-1'>Narxlash
                                </button>
                                {' '}
                                <button onClick={() => this.changeMessageStatus("DESIGNING")}
                                        className='btn btn-dark text-white mx-1'>Dezayn
                                </button>
                                {' '}
                                <button onClick={() => this.changeMessageStatus("CONSULTING")}
                                        className='btn btn-dark text-white mx-1'>Konsalting
                                </button>
                                {' '}
                                <button onClick={() => this.changeMessageStatus("IGNORED")}
                                        className='btn btn-danger text-white mx-1'>Bekor qilish
                                </button>
                                {' '}
                                <button onClick={this.closeModal}
                                        className='btn btn-outline-danger text-black mx-1'><FontAwesomeIcon icon={"window-close"}/>
                                </button>
                                {' '}
                            </div>
                        </ModalFooter>
                    </Modal>

                    <Modal isOpen={ignoredModalVisibility} toggle={this.closeIgnoredModal}>
                        <ModalHeader toggle={this.closeIgnoredModal}>
                            <h3>Sabab nima?</h3>
                        </ModalHeader>
                        <ModalBody>
                            <AvForm onSubmit={this.handleSubmit} id={'form'}>
                                <AvGroup>
                                    <AvInput type="text" id="example" className={"form-control mt-2"}
                                             name={"reason"} required/>
                                    <AvFeedback>Bu maydon bo`sh!</AvFeedback>
                                </AvGroup>
                            </AvForm>
                        </ModalBody>
                        <ModalFooter>
                            <button form={"form"} className={"btn btn-outline-success"}><FontAwesomeIcon icon={"save"}/></button>
                            <button onClick={this.closeIgnoredModal} className={"btn btn-outline-danger"}><FontAwesomeIcon icon={"window-close"}/>
                            </button>
                        </ModalFooter>
                    </Modal>

                    <div className="row">
                        <div className="col-md mb-5 mt-5">
                            <Tabs onSelect={this.handleTab}>

                                <TabList>
                                    <Tab>AKTIV</Tab>
                                    <Tab>NARXLASH</Tab>
                                    <Tab>DEZAYNLASH</Tab>
                                    <Tab>KONSALTING</Tab>
                                    <Tab>BEKOR QILINGAN</Tab>
                                </TabList>

                                <TabPanel>
                                    {
                                        data.length > 0 && data.map(item => {
                                            return (

                                                <div style={item.disabled ? {backgroundColor: "#eee"} : {}}
                                                     key={item.id}
                                                     className={item.disabled ? "card border-bottom-1 mt-2 border-dark"
                                                         : "message card border-bottom-1 mt-2 border-dark"}
                                                     onClick={() => this.openModal(item)}>
                                                    <div className="card-body ">
                                                        {item.text}
                                                        <b>{item.disabled ? <span className={"float-right"}>Menijer ishlamoqda</span> : ""}</b>
                                                    </div>
                                                </div>
                                            )
                                        })
                                    }
                                </TabPanel>
                                <TabPanel>
                                    <div className={"myScroll"} id={'PRICING'} onScroll={this.handleScroll}>
                                        {
                                            myData.map(item => {
                                                return (
                                                    <div key={item.id}
                                                         className="message card border-bottom-1 mt-2 border-dark"
                                                         onClick={() => this.openModal(item)}>
                                                        <div className="card-body ">
                                                            {item.text}
                                                        </div>
                                                    </div>
                                                )
                                            })
                                        }
                                    </div>
                                </TabPanel>
                                <TabPanel>
                                    <div className={"myScroll"} id={'DESIGNING'} onScroll={this.handleScroll}>
                                        {
                                            myData.map(item => {
                                                return (
                                                    <div key={item.id}
                                                         className="message card border-bottom-1 mt-2 border-dark"
                                                         onClick={() => this.openModal(item)}>
                                                        <div className="card-body ">
                                                            {item.text}
                                                        </div>
                                                    </div>
                                                )
                                            })
                                        }
                                    </div>
                                </TabPanel>
                                <TabPanel>
                                    <div className={"myScroll"} id={'CONSULTING'} onScroll={this.handleScroll}>
                                        {
                                            myData.map(item => {
                                                return (
                                                    <div key={item.id}
                                                         className="message card border-bottom-1 mt-2 border-dark"
                                                         onClick={() => this.openModal(item)}>
                                                        <div className="card-body ">
                                                            {item.text}
                                                        </div>
                                                    </div>
                                                )
                                            })
                                        }
                                    </div>
                                </TabPanel>
                                <TabPanel>
                                    <div className={"myScroll"} id={'IGNORED'} onScroll={this.handleScroll}>
                                        {
                                            myData.map(item => {
                                                return (
                                                    <div key={item.id}
                                                         className="message card border-bottom-1 mt-2 border-dark"
                                                         onClick={() => this.openModal(item)}>
                                                        <div className="card-body ">
                                                            {item.text}
                                                        </div>
                                                    </div>
                                                )
                                            })
                                        }
                                    </div>
                                </TabPanel>
                            </Tabs>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Index;