import React, {Component} from 'react';
import axios from 'axios'
import {PATH_PREFIX} from "../../utils";
import {DatePicker} from 'antd';
import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'
import moment from 'moment';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
    faDollarSign,
    faEnvelope,
    faFileInvoice, faInfoCircle,
    faKey, faReceipt,
    faUserPlus,
    faClone,
    faUserTimes, faUserEdit
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt} from "@fortawesome/free-regular-svg-icons";


class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            top: [],
            below: [],
        }
    }

    ddd = () => {
        alert(this.state.currentDate)
    };

    componentDidMount() {
        var date = new Date().toISOString().slice(0, 10).substring(0, 7);
        axios({
            url: PATH_PREFIX + '/api/order/reportTop',
            method: 'get',
            headers: {
                'Authorization': localStorage.getItem('token')
            }
        }).then(res => {
            this.setState({top: res.data.object})
        });

        axios({
            url: PATH_PREFIX + `/api/order/reportBelow?month=${date}`,
            method: 'get',
            headers: {
                'Authorization': localStorage.getItem('token')
            }
        }).then(res => {
            this.setState({below: res.data.object})
        })
    }

    belowFetch = (date, dateString) => {
        axios({
            url: PATH_PREFIX + `/api/order/reportBelow?month=${dateString}`,
            method: 'get',
            headers: {
                'Authorization': localStorage.getItem('token')
            }
        }).then(res => {
            this.setState({below: res.data.object})
        })
    };

    render() {
        library.add(faInfoCircle)
        return (
            <div className={"container mt-4"}>
                <div className="row mt-3 border-bottom">
                    <div className="col-4">
                        <h4 className='text-center' style={{color:'green'}}>{this.state.top[0]}</h4>
                        <p className='text-center'>Umumiy clientlarning puli</p>
                    </div>
                    <div className="col-4">
                        <h4 className='text-center' style={{color:'red'}}>{this.state.top[1] - this.state.top[0]}</h4>
                        <p className='text-center'>Umumiy clientlarning qarzi</p>
                    </div>
                    <div className="col-4">
                        <h4 className='text-center' style={{color:'orange'}}>{this.state.top[2]}</h4>
                        <p className='text-center'>Topshirishi kerak bo'lgan buyurtmalar soni</p>
                    </div>
                </div>
                <div className="row my-3">
                    <div className="m-auto">
                        {
                            <DatePicker onChange={this.belowFetch} picker="month"
                                        defaultValue={moment(new Date().toISOString().slice(0, 10).substring(0, 7), 'YYYY-MM')}/>
                        }
                    </div>
                </div>
                <div className="row">
                    <div className="card col">
                        <div className="card-body">
                            <h4 className='text-center'>{this.state.below[1]}</h4>
                            <p className='text-center'>Qabul qilingan buyurtmalar soni</p>
                        </div>
                    </div>
                    <div className="card col">
                        <div className="card-body">
                            <h4 className='text-center'>{this.state.below[0]}</h4>
                            <p className='text-center'>Tugatilgan buyurtmalar soni</p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Index;