import React, {Component} from 'react';
import {Col, Row} from "antd";
import {AvForm, AvInput} from "availity-reactstrap-validation";
import axios from 'axios';
import {PATH_PREFIX} from "../../../utils";
import {library} from "@fortawesome/fontawesome-svg-core";
import {
    faAt, faEnvelope, faMapMarkerAlt, faPeopleCarry, faPhoneSquareAlt, faSignOutAlt, faUsers, faUserTie
} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {
    faFacebookSquare, faInstagramSquare, faTelegramPlane, faYoutubeSquare,
} from '@fortawesome/free-brands-svg-icons';


class Index extends Component {

    constructor(props) {
        super(props);
        this.state = {
            company: ''
        }
    }

    async componentDidMount() {
        await axios({
            url: PATH_PREFIX + '/api/company',
            method: 'get',
            headers: {
                'Authorization': localStorage.getItem('token')
            }
        }).then(response => {
            if (response.data.success) {
                this.setState({
                    company: response.data.object
                })
            }
        })
    }

    submitForm = (event, errors, values) => {
        if (errors.length === 0) {
            axios({
                url: PATH_PREFIX + '/api/company',
                method: 'post',
                data: values,
                headers: {
                    'Authorization': localStorage.getItem('token')
                }
            })
        }
    };


    render() {
        const {company} = this.state;
        library.add(faEnvelope, faMapMarkerAlt, faSignOutAlt, faAt, faFacebookSquare, faInstagramSquare,
            faTelegramPlane, faYoutubeSquare, faUsers, faPeopleCarry, faUserTie, faPhoneSquareAlt);
        return (
            <div>
                <Row className={"mt-3"}>
                    <Col span={20} offset={2}>
                        <h2 className="text-center my-3 mb-5"><b>Manzil</b></h2>
                        <AvForm onSubmit={this.submitForm} id={"form"}>
                            <Row>
                                <Col span={12}>
                                    <Col span={24} className="px-3">
                                        <div className="input-group mb-3">
                                            <div className="input-group-prepend">
                                                <span className="input-group-text mt-3"><FontAwesomeIcon
                                                    icon={"map-marker-alt"}/>&nbsp;UZ</span>
                                            </div>
                                            <AvInput name={"address"} type={"text"} className={"form-control mt-3"}
                                                     value={company && company.address}
                                                     placeholder={"Manzil o'zbek tilida"}/>
                                        </div>
                                    </Col>
                                    <Col span={24} className="px-3">
                                        <div className="input-group mb-3">
                                            <div className="input-group-prepend">
                                                <span className="input-group-text mt-3"><FontAwesomeIcon
                                                    icon={"map-marker-alt"}/>&nbsp;RU</span>
                                            </div>
                                            <AvInput name={"addressRu"} type={"text"} className={"form-control mt-3"}
                                                     value={company && company.addressRu}
                                                     placeholder={"Manzil rus tilida"}/>
                                        </div>
                                    </Col>
                                    <Col span={24} className="px-3">
                                        <div className="input-group mb-3">
                                            <div className="input-group-prepend">
                                                <span className="input-group-text mt-3"><FontAwesomeIcon
                                                    icon={"at"}/>&nbsp;</span>
                                            </div>
                                            <AvInput name={"email"} type={"text"} className={"form-control mt-3"}
                                                     value={company && company.email} placeholder={"E-mail pochta"}/>
                                        </div>
                                    </Col>
                                    <Col span={24} className="px-3">
                                        <div className="input-group mb-3">
                                            <div className="input-group-prepend">
                                                <span className="input-group-text mt-3"><FontAwesomeIcon
                                                    icon={['fab', 'facebook-square']} size="1.8x"/>&nbsp;</span>
                                            </div>
                                            <AvInput name={"facebook"} type={"text"} className={"form-control mt-3"}
                                                     value={company && company.facebook}
                                                     placeholder={"Facebook sahifasi"}/>
                                        </div>
                                    </Col>
                                    <Col span={24} className="px-3">
                                        <div className="input-group mb-3">
                                            <div className="input-group-prepend">
                                                <span className="input-group-text mt-3"><FontAwesomeIcon
                                                    icon={['fab', 'instagram-square']} size="1.8x"/>&nbsp;</span>
                                            </div>
                                            <AvInput name={"instagram"} type={"text"} className={"form-control mt-3"}
                                                     value={company && company.instagram}
                                                     placeholder={"Instagram sahifasi"}/>
                                        </div>
                                    </Col>
                                    <Col span={24} className="px-3">
                                        <div className="input-group mb-3">
                                            <div className="input-group-prepend">
                                                <span className="input-group-text mt-3"><FontAwesomeIcon
                                                    icon={['fab', 'youtube-square']} size="1.8x"/>&nbsp;</span>
                                            </div>
                                            <AvInput name={"youtube"} type={"text"} className={"form-control mt-3"}
                                                     value={company && company.youtube}
                                                     placeholder={"Youtube sahifasi"}/>
                                        </div>
                                    </Col>
                                    <Col span={24} className="px-3">
                                        <div className="input-group mb-3">
                                            <div className="input-group-prepend">
                                                <span className="input-group-text mt-3"><FontAwesomeIcon
                                                    icon={['fab', 'telegram-plane']} size="1.8x"/>&nbsp;</span>
                                            </div>
                                            <AvInput name={"telegram"} type={"text"} className={"form-control mt-3"}
                                                     value={company && company.telegram}
                                                     placeholder={"Telegram sahifasi"}/>
                                        </div>
                                    </Col>
                                </Col>
                                <Col span={12}>
                                    <Col span={24} className="px-3">
                                        <div className="input-group mb-3">
                                            <div className="input-group-prepend">
                                                <span className="input-group-text mt-3"><FontAwesomeIcon
                                                    icon={"user-tie"}/> &nbsp;&nbsp; soni </span>
                                            </div>
                                            <AvInput name={"masterCount"} type={"number"}
                                                     className={"form-control mt-3"}
                                                     value={company && company.masterCount}
                                                     placeholder={"Masterlar soni"}/>
                                        </div>
                                    </Col>
                                    <Col span={24} className="px-3">
                                        <div className="input-group mb-3">
                                            <div className="input-group-prepend">
                                                <span className="input-group-text mt-3"><FontAwesomeIcon
                                                    icon={"users"}/> &nbsp; soni </span>
                                            </div>
                                            <AvInput name={"customerCount"} type={"number"}
                                                     className={"form-control mt-3"}
                                                     value={company && company.countCustomer}
                                                     placeholder={"Mijoslar soni"}/>
                                        </div>
                                    </Col>
                                    <Col span={24} className="px-3">
                                        <div className="input-group mb-3">
                                            <div className="input-group-prepend">
                                                <span className="input-group-text mt-3"><FontAwesomeIcon
                                                    icon={"people-carry"}/> &nbsp; soni </span>
                                            </div>
                                            <AvInput name={"orderCount"} type={"number"} className={"form-control mt-3"}
                                                     value={company && company.orderCount}
                                                     placeholder={"Buyurtmalar soni"}/>
                                        </div>
                                    </Col>
                                    <Col span={24} className="px-3">
                                        <div className="input-group mb-3">
                                            <div className="input-group-prepend">
                                                <span className="input-group-text mt-3"><FontAwesomeIcon
                                                    icon={"phone-square-alt"}/> &nbsp; 1 </span>
                                            </div>
                                            <AvInput name={"phoneNumber1"} type={"text"} className={"form-control mt-3"}
                                                     value={company && company.phoneNumber1}
                                                     placeholder={"Telefon raqam - 1"}/>
                                        </div>
                                    </Col>
                                    <Col span={24} className="px-3">
                                        <div className="input-group mb-3">
                                            <div className="input-group-prepend">
                                                <span className="input-group-text mt-3"><FontAwesomeIcon
                                                    icon={"phone-square-alt"}/> &nbsp; 2 </span>
                                            </div>
                                            <AvInput name={"phoneNumber2"} type={"text"} className={"form-control mt-3"}
                                                     value={company && company.phoneNumber2}
                                                     placeholder={"Telefon raqam - 2"}/>
                                        </div>
                                    </Col>
                                    <Col span={24} className="px-3">
                                        <div className="input-group mb-3">
                                            <div className="input-group-prepend">
                                                <span className="input-group-text mt-3"><FontAwesomeIcon
                                                    icon={"phone-square-alt"}/> &nbsp; 3 </span>
                                            </div>
                                            <AvInput name={"phoneNumber3"} type={"text"} className={"form-control mt-3"}
                                                     value={company && company.phoneNumber3}
                                                     placeholder={"Telefon raqam - 3"}/>
                                        </div>
                                    </Col>
                                    <Col span={12} offset={14} className="text-align-right mt-4">
                                        <button className="btn btn-outline-dark">O`zgarishlarni saqlash</button>
                                    </Col>
                                </Col>
                            </Row>
                        </AvForm>

                    </Col>
                </Row>
            </div>
        );
    }
}

export default Index;