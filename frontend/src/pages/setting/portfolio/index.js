import React, {Component} from 'react';
import {Card, Col, Container, Row} from "reactstrap";
import axios from 'axios';
import {PATH_PREFIX} from '../../../utils';
import {Popconfirm, Upload} from "antd";
import {PlusOutlined, CloseOutlined} from '@ant-design/icons';
import '../../../global.scss';
import 'antd/dist/antd.css';
import {toast} from "react-toastify";

class Index extends Component {


    componentDidMount() {
        this.getPortfolio();
    }

    constructor(props) {
        super(props);
        this.state = {
            photoId: "",
            portfolios: []
        }
    }

    uploadFile = (options) => {
        const data = new FormData();
        data.append("attachment", options.file);
        axios({
            url: PATH_PREFIX + "/api/file/save",
            method: "post",
            data: data,
            headers: {
                "Content-Type": "multipart/form-data; boundary=----WebKitFormBoundaryqTqJIxvkWFYqvP5s",
                "X-Requested-With": "XMLHttpRequest"
            }

        }).then((payload) => {
            const data1 = new FormData();
            data1.append("attachment", payload.data.id);
            axios({
                url: PATH_PREFIX + '/api/portfolio',
                method: 'post',
                data: data1,
            }).then((response) => {
                if (response.data.success) {
                    this.setState({
                        portfolios: response.data.object
                    });
                    this.getPortfolio();
                }
            })
        })
    };

    getPortfolio = () => {
        axios({
            url: PATH_PREFIX + '/api/portfolio',
            method: 'get',
            headers: {
                'Authorization': localStorage.getItem('token'),
            }
        }).then(function (response) {
            this.setState({
                portfolios: response.data.object,
            })
        }.bind(this));
    };

    beforeUpload = (file) => {
        const isJPG = file.type === "image/jpeg";
        const isPNG = file.type === "image/png";
        if (!isJPG && !isPNG) {
            toast.error("You can only upload JPG or PNG file!")
        }
        return isJPG || isPNG;
    };

    customRequest = (options) => {
        this.uploadFile(options);
    };

    deletePortfolio = (id) => {
        axios({
            url: PATH_PREFIX + "/api/portfolio/" + id,
            method: "delete",
            headers: {
                'Authorization': localStorage.getItem('token'),
            }
        }).then((response) => {
            if (response.data.success) {
                this.getPortfolio();
            }
        })
    };


    render() {

        return (
            <div>
                <Container>
                    <Row className={"mt-5"}>
                        <Col style={{margin: "0 auto"}}><h3>Portfolio qo'shish</h3></Col>
                    </Row>
                    <Row className={"mt-4"}>
                        <Col>
                            <Upload
                                name="attachment"
                                showUploadList={false}
                                beforeUpload={this.beforeUpload}
                                customRequest={this.customRequest}
                            >
                                <Card className="text-center portfolio-card pt-5"
                                      style={{width: "25px", height: "25px"}}>
                                    <PlusOutlined/>
                                    <div className="mt-2 ant-upload-text font-weight-bold">Yuklash</div>
                                </Card>
                            </Upload>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            {/*<div>*/}
                        {this.state.portfolios && this.state.portfolios.map(item =>
                            <Col span={5} className="position-relative portfolio-card mb-5 mt-3 float-left">
                                <Popconfirm className={"delete-portfolio"}
                                            onConfirm={() => this.deletePortfolio(item.id)} title="Ishonchingiz komilmi?"
                                            okText="Ha" cancelText="Yo`q">
                                    <CloseOutlined/>
                                </Popconfirm>
                                <img className="img-fluid"
                                     src={item.attachment && PATH_PREFIX + '/api/file/get/' + item.attachment.id}
                                     alt="avatar"/>
                            </Col>)}
                            {/*</div>*/}
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

export default Index;