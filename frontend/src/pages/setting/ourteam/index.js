import React, {Component} from 'react';
import {AvForm, AvInput, AvGroup, AvFeedback} from "availity-reactstrap-validation";
import {Card, Label, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import {Popconfirm, Upload} from "antd";
import {PlusOutlined} from "@ant-design/icons";
import {toast} from "react-toastify";
import axios from "axios";
import {PATH_PREFIX} from "../../../utils";
import DeleteOutlined from "@ant-design/icons/lib/icons/DeleteOutlined";
import EditOutlined from "@ant-design/icons/lib/icons/EditOutlined";


class Index extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currentMaster: '',
            dataMaster: [],
            modalVisible: false,
            isEdit: false,
            photoId: '',
        }
    }

    componentDidMount() {
        this.getMasters();
    }

    getMasters = () => {
        axios({
            url: PATH_PREFIX + '/api/master',
            method: 'get',
            headers: {
                'Authorization': localStorage.getItem('token')
            }
        }).then((response) => {
            if (response.data.success) {
                this.setState({
                    dataMaster: response.data.object
                })
            }
        })
    };
    showModal = () => {
        this.setState({
            modalVisible: true
        })
    };
    closeModal = () => {
        const {isEdit, photoId} = this.state;
        if (!isEdit) {
            axios({
                url: PATH_PREFIX + '/api/file/'+photoId,
                method: 'delete',
                headers: {
                    'Authorization': localStorage.getItem('token')
                }
            })
        }
        this.setState({
            modalVisible: false,
            isEdit: false,
            photoId: '',
            currentMaster: '',
        });
    };
    handleSubmit = (event, errors, values) => {
        const {currentMaster, isEdit} = this.state;
        if (errors.length === 0) {
            if (!isEdit) {
                const data = {...values, "attachment": this.state.photoId, "active": true};
                axios({
                    url: PATH_PREFIX + '/api/master',
                    method: 'post',
                    data,
                    headers: {
                        'Authorization': localStorage.getItem('token')
                    }
                }).then(response => {
                    if (response.data.success) {
                        this.setState({
                            modalVisible: false,
                            isEdit: false,
                            photoId: '',
                            currentMaster: '',
                        });
                        this.getMasters();
                        toast.success("Master qo'shildi");
                    }
                })
            } else {
                const data = {...values, "id": currentMaster.id, "attachment": this.state.photoId, "active": true};
                axios({
                    url: PATH_PREFIX + '/api/master',
                    method: 'put',
                    data,
                    headers: {
                        'Authorization': localStorage.getItem('token')
                    }
                }).then(response => {
                    if (response.data.success) {
                        this.setState({
                            modalVisible: false,
                            isEdit: false,
                            photoId: '',
                            currentMaster: '',
                        });
                        this.getMasters();
                        toast.success("Master o'zgartirildi");
                    }
                })
            }
        }
    };
    beforeUpload = (file) => {
        const isJPG = file.type === "image/jpeg";
        const isPNG = file.type === "image/png";
        if (!isJPG && !isPNG) {
            toast.error("You can only upload JPG or PNG file!")
        }
        return isJPG || isPNG;
    };
    customRequest = (options) => {
        const data = new FormData();
        data.append("attachment", options.file);
        axios({
            url: PATH_PREFIX + "/api/file/save",
            method: "post",
            data: data,
            headers: {
                "Content-Type": "multipart/form-data; boundary=----WebKitFormBoundaryqTqJIxvkWFYqvP5s",
                "X-Requested-With": "XMLHttpRequest"
            }
        }).then((payload) => {
            this.setState({
                photoId: payload.data.id
            });
        })
    };
    deleteMaster = (id) => {
        axios({
            url: PATH_PREFIX + "/api/master/" + id,
            method: "delete",
            headers: {
                'Authorization': localStorage.getItem('token')
            }
        }).then(response => {
            if (response.data.success) {
                this.getMasters()
            }
        })
    };
    editMaster = (item) => {
        this.setState({
            modalVisible: true,
            isEdit: true,
            currentMaster: item,
            photoId: item.attachment.id
        })
    };

    render() {
        const {currentMaster, dataMaster, modalVisible, isEdit, photoId} = this.state;

        const uploadButton = (
            <Card className="text-center portfolio-card pt-5" style={{width: "20px", height: "20px"}}>
                <PlusOutlined/>
                <div className="mt-2 ant-upload-text font-weight-bold">Yuklash</div>
            </Card>
        );

        return (
            <div>
                <div className="row">
                    <div className="col-md-12">
                        <h2 className="text-center mb-4 mt-5"><b>Masterlar ro'yhati</b></h2>
                    </div>
                </div>
                <div className={"row"}>
                    <div className="col-md-3 offset-1 mr-4">
                        <div onClick={this.showModal} className="btn btn-dark my-3 mb-2">Master qo'shish</div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-10 offset-1">
                        <table className="table">
                            <thead>
                            <tr>
                                <th>TR</th>
                                <th>Rasm</th>
                                <th>Ism</th>
                                <th>Izoh</th>
                                <th>Amallar</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                dataMaster.map((item, index) => {
                                    return (
                                        <tr>
                                            <td>{index + 1}</td>
                                            <td>
                                                <span>
                                                    <div style={{width: "50px", height: "50px", borderRadius: "5px"}}>
                                                        <img className="img-fluid"
                                                             src={PATH_PREFIX+"/api/file/get/" + item.attachment.id}
                                                             alt="Avatar"/>
                                                    </div>
                                                </span>
                                            </td>
                                            <td>{item.fullName}</td>
                                            <td>{item.description}</td>
                                            <td>
                                                <div>
                                                    <Popconfirm className={"delete-portfolio"}
                                                                onConfirm={() => this.deleteMaster(item.id)}
                                                                title="Are you sure？"
                                                                okText="Ha" cancelText="Yoq">
                                                        <DeleteOutlined/>
                                                    </Popconfirm>
                                                    <span className="d-inline-block circle ml-4">
                                                        <EditOutlined onClick={() => this.editMaster(item)}/>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                    )
                                })
                            }
                            </tbody>
                        </table>
                    </div>
                </div>
                <Modal isOpen={modalVisible} toggle={this.closeModal}>
                    <ModalHeader toggle={this.closeModal} className={"bg-light text-white"}>
                        {!isEdit ? "Qo'shish" : "O'zgartirish"}
                    </ModalHeader>
                    <ModalBody>
                        <div className="row">
                            <div className="col-md-4 mt-4">
                                <Upload
                                    name="attachment"
                                    showUploadList={false}
                                    beforeUpload={this.beforeUpload}
                                    customRequest={this.customRequest}
                                >
                                    {photoId ?
                                        <img src={PATH_PREFIX+"/api/file/get/" + photoId} alt="avatar"
                                             style={{width: '100%'}}/>
                                        : uploadButton}
                                </Upload>
                            </div>
                            <div className="col-md-8">
                                <AvForm onSubmit={this.handleSubmit} id={"form"}>
                                    <AvGroup>
                                        <Label for={"example"}>Ism familiya</Label>
                                        <AvInput name={"name"} type={"text"} className={"pt-0 pb-0 mt-5"}
                                                 value={currentMaster && currentMaster.fullName}
                                                 id={"example"}
                                                 required/>
                                        <AvFeedback>Iltimos, manager familiyasini kiriting!</AvFeedback>
                                    </AvGroup>
                                    <AvGroup>
                                        <Label for={"example1"}>Lavozimi</Label>
                                        <AvInput name={"description"} type={"text"} className={"form-control mt-5"}
                                                 value={currentMaster && currentMaster.description}
                                                 id={"example1"}
                                                 required/>
                                        <AvFeedback>Iltimos, manager lavozimini kiriting!</AvFeedback>
                                    </AvGroup>
                                </AvForm>
                            </div>
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        <button form={"form"} className={"btn btn-outline-success"}>Saqlash</button>
                        <button onClick={this.closeModal} className={"btn btn-outline-secondary"}>Bekor</button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default Index;