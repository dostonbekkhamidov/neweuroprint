import React, {Component} from 'react';
import axios from "axios";
import {PATH_PREFIX} from "../../utils";
import {Tab, Tabs, TabList, TabPanel} from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";
import OrderModal from "../../components/OrderModal";
import Pagination from "react-js-pagination";
import 'font-awesome/css/font-awesome.min.css';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
    faDollarSign,
    faEnvelope,
    faFileInvoice, faInfoCircle,
    faReceipt,
    faUserPlus,
    faClone,
    faUserTimes, faUserEdit
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt} from "@fortawesome/free-regular-svg-icons";

class Index extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            productList: [],
            inputRow: [],
            orderSum: '',
            paySum: '',
            currentTab: 'PENDING',
            currentPage: 1,
            currentSize: 10,
            activePage: 1,
            totalElements: 0,
            modalVisibility: false,
            orderVisibility: false,
            orderId: '',
            modalType: '',
            page: 'order',
            currentMessage: '',
            clientId: '',
            messageId: '',
            currentOrderId: '',
        }
    }

    componentDidMount() {
        this.pendingOrder();
    }

    pendingOrder = () => {
        axios({
            url: PATH_PREFIX + '/api/order',
            method: 'get',
            params: {
                status: this.state.currentTab,
                page: this.state.currentPage,
                size: this.state.currentSize,
            },
            headers: {
                'Authorization': localStorage.getItem('token')
            }
        }).then(response => {
            this.setState({
                data: response.data.object,
                totalElements: response.data.totalElements,
            });
        })
    };
    handleTab = (index) => {
        let param = '';
        this.handlePageChange(1);
        // eslint-disable-next-line default-case
        switch (index) {
            case 0:
                param = 'PENDING';
                break;
            case 1:
                param = 'IN_PROCESS';
                break;
            case 2:
                param = 'COMPLETED';
                break;
            case 3:
                param = 'ARCHIVE';
                break;
            case 4:
                param = 'IGNORED';
                break;
        }
        this.setState({
            currentTab: param,
            currentSize: 10,
        });
        axios({
            url: PATH_PREFIX + '/api/order',
            method: 'get',
            params: {
                status: param,
                page: 1,
                size: this.state.currentSize,
            },
            headers: {
                'Authorization': localStorage.getItem("token")
            }
        }).then(response => {
            this.setState({
                data: response.data.object,
                totalElements: response.data.totalElements,
            })
        })
    };
    info = (id) => {
        this.getOrderSum(id);
        this.getClientBalance(id);
        this.orderProducts(id);
        this.setState({
            modalVisibility: true,
            modalType: "inf",
            orderId: id,
        })
    };
    openOrderCloneModal = (item) => {
        this.orderProducts(item.id);
        this.setState({
            modalVisibility: true,
            modalType: "clone",
            orderId: item.id,
            clientId: item.clientId
        });
    };
    cloneOrder = () => {
        axios({
            url: PATH_PREFIX + "/api/order",
            method: 'post',
            data: {
                products: this.state.productList,
                isBot: false,
                clientId: this.state.clientId,
                messageId: null,
            },
            headers: {
                'Authorization': localStorage.getItem('token')
            }
        }).then((response) => {
            if (response.data.success) {
                this.setState({
                    modalVisibility: false,
                    modalType: "",
                    orderId: "",
                    clientId: "",
                });
                this.pendingOrder();
            }
        })
    };
    getOrderSum = (orderId) => {
        axios({
            url: PATH_PREFIX + '/api/product/' + orderId,
            method: 'get',
            headers: {
                'Authorization': localStorage.getItem('token')
            }
        }).then(response => {
            this.setState({
                orderSum: response.data.object
            })
        });
    };
    getClientBalance = (id) => {
        axios({
            url: PATH_PREFIX + '/api/client/balance/' + id,
            method: 'get',
            headers: {
                'Authorization': localStorage.getItem('token')
            }
        }).then(response => {
            this.setState({
                paySum: response.data.object,
            })
        });
    };
    closeModal = () => {
        this.setState({
            orderSum: '',
            paySum: '',
            productList: [],
            modalVisibility: false,
            orderId: '',
        })
    };
    orderProducts = (id) => {
        axios({
            url: PATH_PREFIX + '/api/product/me/' + id,
            method: 'get',
            headers: {
                'Authorization': localStorage.getItem('token')
            }
        }).then((response) => {
            this.setState({
                productList: response.data.object,
            })
        });
    };
    changeOrderStatus = (orderId, status) => {
        const {currentTab} = this.state;
        axios({
            url: PATH_PREFIX + '/api/order/' + orderId,
            method: 'patch',
            params: {
                status
            },
            headers: {
                'Authorization': localStorage.getItem('token')
            }
        }).then((response) => {
            if (response.data.success) {
                axios({
                    url: PATH_PREFIX + '/api/order',
                    method: 'get',
                    params: {
                        status: currentTab,
                        page: 1,
                        size: this.state.currentSize,
                    },
                    headers: {
                        'Authorization': localStorage.getItem('token')
                    }
                }).then(response => {
                    this.setState({
                        data: response.data.object,
                    });
                })
            }
        });
        this.closeModal()
    };
    handlePageChange = (pageNumber) => {
        this.setState({
            currentPage: pageNumber
        });
        axios({
            url: PATH_PREFIX + '/api/order',
            method: 'get',
            params: {
                status: this.state.currentTab,
                page: pageNumber,
                size: 10,
            },
            headers: {
                'Authorization': localStorage.getItem('token')
            }
        }).then(response => {
            this.setState({
                data: response.data.object,
                totalElements: response.data.totalElements,
            });
        })
    };
    closeOrderModal = () => {
        this.setState({
            orderVisibility: false,
            productList: [],
        })
    };
    edit = (id) => {
        axios({
            url: PATH_PREFIX + '/api/order/' + id,
            method: 'get',
            headers: {
                'Authorization': localStorage.getItem('token')
            }
        }).then(response => {
            if (response.data.success) {
                this.setState({
                    inputRow: response.data.object,
                    currentMessage: response.data.currentMessage,
                    currentOrderId: id,
                    orderVisibility: true,
                })
            }
        })
    };
    saveOrder = (products) => {
        const {currentMessage, currentOrderId} = this.state;
        axios({
            url: PATH_PREFIX + '/api/order/edit/' + currentOrderId,
            method: 'patch',
            data: {
                products,
                isBot: true,
                messageId: currentMessage !== null ? currentMessage.id : null,
                clientId: currentMessage !== null ? currentMessage.client.id : null,
            },
            headers: {
                'Authorization': localStorage.getItem('token')
            }
        }).then((response) => {
            if (response.data.success) {
                this.setState({
                    orderVisibility: false,
                    productList: [],
                    currentMessage: '',
                    currentOrderId: '',
                });
                this.pendingOrder();
            }
        });
    };
    addProduct = () => {
        const {inputRow} = this.state;
        inputRow.push({
            productName: '',
            threeDSize: '',
            material: '',
            knife: '',
            process: '',
            type: '',
            price: '',
            amount: '',
        });
        this.setState({
            inputRow
        })
    };
    deleteRow = (index) => {
        const {inputRow} = this.state;
        inputRow.splice(index, 1);
        this.setState({
            inputRow,
            orderVisibility: true
        })
    };

    render() {
        const {
            data, orderSum, paySum, productList, modalVisibility, orderId, inputRow, orderVisibility,
            modalType, page, currentTab, totalElements, currentPage
        } = this.state;

        library.add(faEnvelope,faDollarSign,faFileInvoice,faUserTimes,faUserPlus,faInfoCircle,faTrashAlt,
            faReceipt,faClone,faUserEdit);

        return (
            <div>
                <OrderModal
                    inputRow={inputRow}
                    addProduct={this.addProduct}
                    closeOrderModal={this.closeOrderModal}
                    deleteRow={this.deleteRow}
                    orderVisibility={orderVisibility}
                    saveOrder={this.saveOrder}
                    page={page}
                />
                <Modal isOpen={modalVisibility} toggle={this.closeModal} className={"orderModal"}>
                    <ModalHeader toggle={this.closeModal} className={"bg-light text-white"}>
                        {modalType === "inf" ? <div><p><b>Buyurtma narxi: </b>{orderSum}</p>
                            <p><b>Balans: </b>{paySum}</p>
                        </div> : <p>Buyurtmani klonlashtirish</p>
                        }
                    </ModalHeader>
                    <ModalBody className={"bg-light"}>
                        <div className="row">
                            <div className="col">
                                <table className={"table"}>
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Maxsulot</th>
                                        <th>Narxi</th>
                                        <th>Miqdori</th>
                                        <th>Jami</th>
                                        <th>Bajarigan sana</th>
                                        <th>Yangilangan sana</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {
                                        productList.map((item, index) => {
                                            return (
                                                <tr key={index}>
                                                    <td>{index + 1}</td>
                                                    <td>{item.productName}</td>
                                                    <td>{item.price}</td>
                                                    <td>{item.amount}</td>
                                                    <td>{item.price * item.amount}</td>
                                                    <td>{item.createdAt.substring(0, 10)}</td>
                                                    <td>{item.updatedAt.substring(0, 10)}</td>
                                                </tr>
                                            )
                                        })
                                    }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </ModalBody>
                    {modalType === "inf" ? <ModalFooter className={" bg-light"}>
                        <div className="row">
                            <button onClick={() => this.changeOrderStatus(orderId, "IN_PROCESS")}
                                    className='btn btn-dark text-white mx-1'>Jarayonda
                            </button>
                            {' '}
                            <button onClick={() => this.changeOrderStatus(orderId, "COMPLETED")}
                                    className='btn btn-dark text-white mx-1'>Tugallandi
                            </button>
                            {' '}
                            <button onClick={() => this.changeOrderStatus(orderId, "ARCHIVE")}
                                    className='btn btn-dark text-white mx-1'>Arxivlash
                            </button>
                            {' '}
                            <button onClick={() => this.changeOrderStatus(orderId, "IGNORED")}
                                    className='btn btn-danger text-white mx-1'>Bekor qilish
                            </button>
                            {' '}
                            <button onClick={this.closeModal}
                                    className='btn btn-outline-danger text-black mx-1'><FontAwesomeIcon icon={"window-close"}/>
                            </button>
                            {' '}
                        </div>
                    </ModalFooter> : <ModalFooter className={" bg-light"}>
                        <Button onClick={this.cloneOrder} className='btn btn-outline-dark'><FontAwesomeIcon icon={"clone"}/></Button>
                        <Button onClick={this.closeModal} className='btn btn-outline-danger'><FontAwesomeIcon icon={"window-close"}/></Button>
                    </ModalFooter>}
                </Modal>
                <div className="container">
                    <div className="row mt-5">
                        <div className="col-md">
                            <Tabs onSelect={this.handleTab}>
                                <TabList>
                                    <Tab>NAVBATDA</Tab>
                                    <Tab>JARAYONDA</Tab>
                                    <Tab>TUGALLANGAN</Tab>
                                    <Tab>ARXIV</Tab>
                                    <Tab>BEKOR QILINGAN</Tab>
                                </TabList>
                                <TabPanel>
                                    <div id={'PENDING'}>
                                        <table className={"table table-striped table-hover mt-4"}>
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Mijoz</th>
                                                <th>Telefon</th>
                                                <th>Sana</th>
                                                <th>Summa</th>
                                                <th>To`lov holati</th>
                                                <th>Ma'lumot</th>
                                                <th>Klonlash</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {
                                                data.map((item, index) => {
                                                    return (
                                                        <tr key={index}>
                                                            <td>{index + 1 + (currentPage - 1) * 10}</td>
                                                            <td>{item.fullName !== null ? item.fullName : 'N/A'}</td>
                                                            <td>{item.phoneNumber}</td>
                                                            <td>{item.createdAt}</td>
                                                            <td>{item.sum}</td>
                                                            <td>{item.orderPayStatus}</td>
                                                            <td>
                                                                <button onClick={() => this.info(item.id)}
                                                                        className={"btn btn-outline-dark"}><FontAwesomeIcon icon={"info-circle"}/>
                                                                </button>
                                                            </td>
                                                            <td>
                                                                <button onClick={() => this.openOrderCloneModal(item)}
                                                                        className={"btn btn-outline-dark"}><FontAwesomeIcon icon={"clone"}/>
                                                                </button>
                                                            </td>
                                                            {
                                                                currentTab === 'PENDING' ?
                                                                    <td>
                                                                        <button onClick={() => this.edit(item.id)}
                                                                                className={"btn btn-outline-dark"}><FontAwesomeIcon icon={"user-edit"}/>
                                                                        </button>
                                                                    </td> : ''
                                                            }
                                                        </tr>
                                                    )
                                                })
                                            }
                                            </tbody>
                                        </table>
                                    </div>
                                    {
                                        totalElements !== 0 ?
                                            <Pagination
                                                activePage={currentPage}
                                                itemsCountPerPage={10}
                                                totalItemsCount={totalElements}
                                                nextPageText={">"}
                                                prevPageText={"<"}
                                                onChange={this.handlePageChange}

                                            />
                                            : ''
                                    }
                                </TabPanel>
                                <TabPanel>
                                    <div id={'IN_PROCESS'}>
                                        <table className={"table table-striped table-hover mt-4"}>
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Mijoz</th>
                                                <th>Telefon</th>
                                                <th>Sana</th>
                                                <th>Summa</th>
                                                <th>To`lov holati</th>
                                                <th>Ma'lumot</th>
                                                <th>Klonlash</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {
                                                data.map((item, index) => {
                                                    return (
                                                        <tr key={index}>
                                                            <td>{index + 1 + (currentPage - 1) * 10}</td>
                                                            <td>{item.fullName !== null ? item.fullName : 'N/A'}</td>
                                                            <td>{item.phoneNumber}</td>
                                                            <td>{item.createdAt}</td>
                                                            <td>{item.sum}</td>
                                                            <td>{item.orderPayStatus}</td>
                                                            <td>
                                                                <button onClick={() => this.info(item.id)}
                                                                        className={"btn btn-outline-dark"}><FontAwesomeIcon icon={"info-circle"}/>
                                                                </button>
                                                            </td>
                                                            <td>
                                                                <button onClick={() => this.openOrderCloneModal(item)}
                                                                        className={"btn btn-outline-dark"}><FontAwesomeIcon icon={"clone"}/>
                                                                </button>
                                                            </td>
                                                        </tr>
                                                    )
                                                })
                                            }
                                            </tbody>
                                        </table>
                                    </div>
                                    {
                                        totalElements !== 0 ?
                                            <Pagination
                                                activePage={currentPage}
                                                itemsCountPerPage={10}
                                                totalItemsCount={totalElements}
                                                nextPageText={">"}
                                                prevPageText={"<"}
                                                onChange={this.handlePageChange}
                                            />
                                            : ''
                                    }
                                </TabPanel>
                                <TabPanel>
                                    <div id={'COMPLETED'}>
                                        <table className={"table table-striped table-hover mt-4"}>
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Mijoz</th>
                                                <th>Telefon</th>
                                                <th>Sana</th>
                                                <th>Summa</th>
                                                <th>To`lov holati</th>
                                                <th>Ma'lumot</th>
                                                <th>Klonlash</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {
                                                data.map((item, index) => {
                                                    return (
                                                        <tr key={index}>
                                                            <td>{index + 1 + (currentPage - 1) * 10}</td>
                                                            <td>{item.fullName !== null ? item.fullName : 'N/A'}</td>
                                                            <td>{item.phoneNumber}</td>
                                                            <td>{item.createdAt}</td>
                                                            <td>{item.sum}</td>
                                                            <td>{item.orderPayStatus}</td>
                                                            <td>
                                                                <button onClick={() => this.info(item.id)}
                                                                        className={"btn btn-outline-dark"}><FontAwesomeIcon icon={"info-circle"}/>
                                                                </button>
                                                            </td>
                                                            <td>
                                                                <button onClick={() => this.openOrderCloneModal(item)}
                                                                        className={"btn btn-outline-dark"}><FontAwesomeIcon icon={"clone"}/>
                                                                </button>
                                                            </td>
                                                        </tr>
                                                    )
                                                })
                                            }
                                            </tbody>
                                        </table>
                                    </div>
                                    {
                                        totalElements !== 0 ?
                                            <Pagination
                                                activePage={currentPage}
                                                itemsCountPerPage={10}
                                                totalItemsCount={totalElements}
                                                nextPageText={">"}
                                                prevPageText={"<"}
                                                onChange={this.handlePageChange}
                                            />
                                            : ''
                                    }
                                </TabPanel>
                                <TabPanel>
                                    <div id={'ARCHIVE'}>
                                        <table className={"table table-striped table-hover mt-4"}>
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Mijoz</th>
                                                <th>Telefon</th>
                                                <th>Sana</th>
                                                <th>Summa</th>
                                                <th>To`lov holati</th>
                                                <th>Ma'lumot</th>
                                                <th>Klonlash</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {
                                                data.map((item, index) => {
                                                    return (
                                                        <tr key={index}>
                                                            <td>{index + 1 + (currentPage - 1) * 10}</td>
                                                            <td>{item.fullName !== null ? item.fullName : 'N/A'}</td>
                                                            <td>{item.phoneNumber}</td>
                                                            <td>{item.createdAt}</td>
                                                            <td>{item.sum}</td>
                                                            <td>{item.orderPayStatus}</td>
                                                            <td>
                                                                <button onClick={() => this.info(item.id)}
                                                                        className={"btn btn-outline-dark"}><FontAwesomeIcon icon={"info-circle"}/>
                                                                </button>
                                                            </td>
                                                            <td>
                                                                <button onClick={() => this.openOrderCloneModal(item)}
                                                                        className={"btn btn-outline-dark"}><FontAwesomeIcon icon={"clone"}/>
                                                                </button>
                                                            </td>
                                                        </tr>
                                                    )
                                                })
                                            }
                                            </tbody>
                                        </table>
                                    </div>
                                    {
                                        totalElements !== 0 ?
                                            <Pagination
                                                activePage={currentPage}
                                                itemsCountPerPage={10}
                                                totalItemsCount={totalElements}
                                                nextPageText={">"}
                                                prevPageText={"<"}
                                                onChange={this.handlePageChange}
                                            />
                                            : ''
                                    }
                                </TabPanel>
                                <TabPanel>
                                    <div id={'IGNORED'}>
                                        <table className={"table table-striped table-hover mt-4"}>
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Mijoz</th>
                                                <th>Telefon</th>
                                                <th>Sana</th>
                                                <th>Summa</th>
                                                <th>To`lov holati</th>
                                                <th>Ma'lumot</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {
                                                data.map((item, index) => {
                                                    return (
                                                        <tr key={index}>
                                                            <td>{index + 1 + (currentPage - 1) * 10}</td>
                                                            <td>{item.fullName !== null ? item.fullName : 'N/A'}</td>
                                                            <td>{item.phoneNumber}</td>
                                                            <td>{item.createdAt}</td>
                                                            <td>{item.sum}</td>
                                                            <td>{item.orderPayStatus}</td>
                                                            <td>
                                                                <button onClick={() => this.info(item.id)}
                                                                        className={"btn btn-outline-dark"}><FontAwesomeIcon icon={"info-circle"}/>
                                                                </button>
                                                            </td>
                                                        </tr>
                                                    )
                                                })
                                            }
                                            </tbody>
                                        </table>
                                    </div>
                                    {
                                        totalElements !== 0 ?
                                            <Pagination
                                                activePage={currentPage}
                                                itemsCountPerPage={10}
                                                totalItemsCount={totalElements}
                                                nextPageText={">"}
                                                prevPageText={"<"}
                                                onChange={this.handlePageChange}
                                            />
                                            : ''
                                    }
                                </TabPanel>
                            </Tabs>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Index;