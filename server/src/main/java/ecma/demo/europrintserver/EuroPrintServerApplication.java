package ecma.demo.europrintserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EuroPrintServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(EuroPrintServerApplication.class, args);
    }

}
