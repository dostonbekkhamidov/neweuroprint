package ecma.demo.europrintserver.bot;

import ecma.demo.europrintserver.bot.service.ButtonService;
import ecma.demo.europrintserver.bot.service.TgService;
import ecma.demo.europrintserver.entity.*;
import ecma.demo.europrintserver.payload.ReqOneRowButtons;
import ecma.demo.europrintserver.repository.*;
import ecma.demo.europrintserver.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class MyBot extends TelegramLongPollingBot {

    @Autowired
    ButtonService myButtons;
    @Autowired
    TgMessageRepository tgMessageRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    ClientRepository clientRepository;
    @Autowired
    TelegramStateRepository telegramStateRepository;
    @Autowired
    TgService tgService;
    @Autowired
    MessageRepository messageRepository;
    @Autowired
    MessageService messageService;
    @Autowired
    MessageDefaultRepository messageDefaultRepository;
    @Autowired
    SimpMessageSendingOperations simpMessageSendingOperations;
    @Autowired
    OrderRepository orderRepository;


    @Override
    public String getBotToken() {
        return "852107889:AAE08FSOeQLt9dDIFReXiUEO3oQXYaz5T0Y";
    }

    @Override
    public String getBotUsername() {
        return "Europrintsalesbot";
    }

    @Override
    public void onUpdateReceived(Update req) {

        if (req.hasMessage()) {
            Long chatId = req.getMessage().getChatId();

            if (req.getMessage().hasText()) {
                String text = req.getMessage().getText();

                if (text.equals("/start")) {
                    Optional<Client> oClient = clientRepository.findByTgId(chatId);
                    if (oClient.isPresent()) {
                        tgService.changeState(chatId, BotState.REGISTERED);
                        send(tgService.homePage(chatId));
                    } else {
                        SendMessage sendMessage = new SendMessage();
                        sendMessage.setChatId(chatId);
                        sendMessage.setText("Assalamu alaykum. Iltimos botdan to'liq foydalanish uchun raqamingizni jo'nating ");
                        sendMessage.setReplyMarkup(myButtons.contactButton());
                        tgService.changeState(chatId, BotState.FIRST_STATE);
                        send(sendMessage);
                    }
                    deleteClientMessage(req);
                } else {
                    Optional<TelegramState> byTgId = telegramStateRepository.findByTgId(chatId);
                    if (byTgId.isPresent()) {
                        String myState = byTgId.get().getTgState();
                        if (myState.equals(BotState.FIRST_STATE)) {
                            tgService.onOutRule(req, "Iltimos Share Contact tugmasini bosing");
                        } else if (myState.equals(BotState.REGISTERED)) {
                            tgService.onOutRule(req, "Iltimos yuqoridagilardan birini tanlang");
                        } else if (myState.equals(BotState.SHOWED_BALANCE)) {
                            tgService.onOutRule(req, "Iltimos bosh menyuga qaytish tugmasini bosing");
                        } else if (myState.equals(BotState.SHOWED_CLIENT_ORDERS)) {
                            tgService.onOutRule(req, "Iltimos oldingi buyurtmalardan birini tanlang yoki yangi buyurtma bering");
                        } else if (myState.equals(BotState.NEW_ORDER_DESC_ENTERING_FINISHED)) {
                            tgService.onOutRule(req, "Iltimos bosh menyuga qaytish tugmasini bosing");
                        } else if (myState.equals(BotState.NEW_ORDER_DESC_ENTERING)) {
                            delete(chatId);
                            deleteClientMessage(req);

                            Client client = clientRepository.findByTgId(chatId).get();
                            List<MessageDefault> messageDefaultList = messageDefaultRepository.findAllByClientId(client.getId());
                            if (messageDefaultList.size() == 10) {
                                SendMessage sendMessage1 = new SendMessage();
                                sendMessage1.setText("Boshqa ma'lumot yuborish imkoniyati yo'q. Iltimos jarayonni yakunlang!");
                                sendMessage1.setChatId(chatId);
                                sendMessage1.setReplyMarkup(myButtons.myButton("Bosh menuga qaytish", BotConstant.BACK_TO_MAIN_FROM_SEND_MESSAGE));
                                send(sendMessage1);
                                DeleteMessage deleteMessage = new DeleteMessage();
                                deleteMessage.setChatId(chatId);
                                deleteMessage.setMessageId(req.getMessage().getMessageId());
                                try {
                                    execute(deleteMessage);
                                } catch (TelegramApiException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                SendMessage sendMessage = new SendMessage();
                                sendMessage.setText("Yozishda davom eting yoki jarayonni yakunlang!");
                                sendMessage.setChatId(chatId);
                                sendMessage.setReplyMarkup(myButtons.myButton("Jarayonni yakunlash va Bosh menuga qaytish", BotConstant.BACK_TO_MAIN_FROM_SEND_MESSAGE));
                                send(sendMessage);
                                messageDefaultRepository.save(new MessageDefault(client, text));
                            }
                        }
                    }
                }
            }
            if (req.getMessage().hasContact()) {
                String phoneNumber = req.getMessage().getContact().getPhoneNumber();

                TelegramState telegramState = telegramStateRepository.findByTgId(req.getMessage().getChatId()).get();
                clientRepository.save(new Client(null, phoneNumber, telegramState, chatId));
                delete(req.getMessage().getChatId());
                deleteClientMessage(req);
                send(tgService.homePage(chatId));
            }
        }


        if (req.hasCallbackQuery()) {

            Long chatId = req.getCallbackQuery().getMessage().getChatId();
            String data = req.getCallbackQuery().getData();

            if (data.equals(BotConstant.GET_BALANCE)) {
                delete(chatId);
                SendMessage sendMessage = new SendMessage();
//                sendMessage.setText("<b>" + clientService.getClientBalanceForBot(chatId.intValue()) + "</b>");
                sendMessage.setChatId(chatId);
                sendMessage.setReplyMarkup(myButtons.myButton("\uD83C\uDFE0", BotConstant.BACK_TO_MAIN));
                send(sendMessage);
                tgService.changeState(chatId, BotState.SHOWED_BALANCE);
            } else if (data.equals(BotConstant.BACK_TO_MAIN)) {
                send(tgService.homePage(chatId));
            } else if (data.equals(BotConstant.MY_ORDERS)) {
                tgService.sendClientOrders(chatId);
                SendMessage sendMessage = new SendMessage();
                sendMessage.setChatId(chatId);
                List<ReqOneRowButtons> reqOneRowButtons = new ArrayList<>();
                reqOneRowButtons.add(new ReqOneRowButtons("Bosh menu", BotConstant.BACK_TO_MAIN));
                reqOneRowButtons.add(new ReqOneRowButtons("===>", BotConstant.NEW_ORDER_CLICKED));
                sendMessage.setReplyMarkup(myButtons.oneRowButtons(reqOneRowButtons));
                sendMessage.setText("Yangi buyurtma ");
                send(sendMessage);
                tgService.changeState(chatId, BotState.SHOWED_CLIENT_ORDERS);
            } else if (data.equals(BotConstant.BACK_TO_MAIN_FROM_SEND_MESSAGE)) {

                Client client = clientRepository.findByTgId(chatId).get();
                String textForMessage = "";
                List<MessageDefault> messageDefaults = messageDefaultRepository.findAllByClientId(client.getId());
                for (MessageDefault messageDefault : messageDefaults) {
                    textForMessage = textForMessage + " " + messageDefault.getText();
                }
                messageService.generateMessageForNewOrder(textForMessage, chatId);
                tgService.changeState(chatId, BotState.NEW_ORDER_DESC_ENTERING_FINISHED);
                tgService.clearBuffer(client.getId());
                send(tgService.homePage(chatId));
            } else if (data.equals(BotConstant.NEW_ORDER_CLICKED)) {
                delete(chatId);
                SendMessage sendMessage = new SendMessage();
                sendMessage.setText("Xabaringizni qoldiring. (Ushbu xabarda o'zingizga kerak bo'ladigan mahsulot haqida batafsil yozishingiz mumkin)");
                sendMessage.setChatId(chatId);
                send(sendMessage);
                tgService.changeState(chatId, BotState.NEW_ORDER_DESC_ENTERING);
            } else {
                delete(chatId);
                SendMessage sendMessage = new SendMessage();
                List<ReqOneRowButtons> reqOneRowButtons = new ArrayList<>();
                reqOneRowButtons.add(new ReqOneRowButtons("Bosh menu", BotConstant.BACK_TO_MAIN));
                sendMessage.setText("Eski buyurtma bo'yicha xabar qoldirildi");
                sendMessage.setChatId(chatId);
                sendMessage.setReplyMarkup(myButtons.oneRowButtons(reqOneRowButtons));
                send(sendMessage);
                messageService.generateMessageForOldOrder("Eski buyurtma asosida", chatId, UUID.fromString(data));
            }
        }
    }


    public void send(SendMessage sendMessage) {
        try {
            sendMessage.setParseMode(ParseMode.HTML);
            org.telegram.telegrambots.meta.api.objects.Message message = execute(sendMessage);
            tgMessageRepository.save(new TgMessage(Long.parseLong(sendMessage.getChatId()), message.getMessageId()));
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    public void delete(Long chatId) {
        List<TgMessage> allByChatId = tgMessageRepository.findAllByTgId(chatId);
        for (TgMessage tgMessage : allByChatId) {
            DeleteMessage deleteMessage = new DeleteMessage();
            deleteMessage.setMessageId(tgMessage.getMessageId());
            deleteMessage.setChatId(chatId);
            try {
                execute(deleteMessage);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }
        tgMessageRepository.deleteAll(allByChatId);
    }

    public void deleteClientMessage(Update req) {
        DeleteMessage deleteMessage = new DeleteMessage();
        deleteMessage.setChatId(req.getMessage().getChatId());
        deleteMessage.setMessageId(req.getMessage().getMessageId());
        try {
            execute(deleteMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

}
