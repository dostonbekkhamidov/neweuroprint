package ecma.demo.europrintserver.bot.service;

import ecma.demo.europrintserver.bot.BotConstant;
import ecma.demo.europrintserver.bot.BotState;
import ecma.demo.europrintserver.bot.MyBot;
import ecma.demo.europrintserver.entity.*;
import ecma.demo.europrintserver.payload.ReqOneRowButtons;
import ecma.demo.europrintserver.repository.ClientRepository;
import ecma.demo.europrintserver.repository.MessageDefaultRepository;
import ecma.demo.europrintserver.repository.OrderRepository;
import ecma.demo.europrintserver.repository.TelegramStateRepository;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class TgService {

    @Autowired
    TelegramStateRepository telegramStateRepository;
    @Autowired
    MyBot myBot;
    @Autowired
    ButtonService buttonService;
    @Autowired
    ClientRepository clientRepository;
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    MessageDefaultRepository messageDefaultRepository;

    public void clearBuffer(UUID clientId){
        List<MessageDefault> messageDefaults = messageDefaultRepository.findAllByClientId(clientId);
        for (MessageDefault messageDefault : messageDefaults) {
            messageDefaultRepository.delete(messageDefault);
        }
    }

    public SendMessage homePage(Long chatId) {
        changeState(chatId, BotState.REGISTERED);
        myBot.delete(chatId);
        SendMessage sendMessage = new SendMessage();
        sendMessage.setText("<b>********Home page********</b>");
        sendMessage.setChatId(chatId);
        sendMessage.setParseMode(ParseMode.HTML);
        sendMessage.setReplyMarkup(buttonService.homeButton());
        return sendMessage;
    }

    public void changeState(Long chatId, String state) {
        Optional<TelegramState> telegramState = telegramStateRepository.findByTgId(chatId);
        if (telegramState.isPresent()) {
            telegramState.get().setTgState(state);
            telegramStateRepository.save(telegramState.get());
        } else {
            telegramStateRepository.save(new TelegramState(chatId, state));
        }
    }

    public List<ReqOneRowButtons> sendClientOrders(Long chatId) {
        myBot.delete(chatId);
        Optional<Client> byTelegramId = clientRepository.findByTgId(chatId);

        var productInfo = "";
        if (byTelegramId.isPresent()) {
            Page<Order> allByClientIdOrderByCreatedAtDesc = orderRepository.findAllByClientIdOrderByCreatedAtDesc(PageRequest.of(0, 10), byTelegramId.get().getId());
            List<Order> orders = allByClientIdOrderByCreatedAtDesc.getContent();
            List<ReqOneRowButtons> reqOneRowButtons = new ArrayList<>();
            for (Order order : orders) {


                for (Product product : order.getProducts()) {
                    productInfo=productInfo+"<b>Mahsulot:</b> "+product.getProductName()+", <b>Soni:</b> "+product.getAmount()+", <b>Narxi:</b> "+
                    product.getPrice()+"; ";
                }
                reqOneRowButtons.add(new ReqOneRowButtons("Reorder with this products ☝ ",order.getId().toString()));
                SendMessage sendMessage = new SendMessage();
                sendMessage.setReplyMarkup(buttonService.myButton("Reorder with this products ☝ ",order.getId().toString()));
                sendMessage.setText(productInfo);
                productInfo = "";
                sendMessage.setChatId(chatId);
                myBot.send(sendMessage);

            }
            return reqOneRowButtons;
        }
        return null;
    }

    public void onOutRule(Update req, String text) {
        myBot.deleteClientMessage(req);
        SendMessage sendMessage = new SendMessage();
        sendMessage.setText(text);
        sendMessage.setChatId(req.getMessage().getChatId());
        myBot.send(sendMessage);
    }

}
