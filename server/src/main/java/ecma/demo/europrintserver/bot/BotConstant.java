package ecma.demo.europrintserver.bot;

public interface BotConstant {

    String BACK_TO_MAIN = "BACK_TO_MAIN";
    String BACK_TO_MAIN_FROM_SEND_MESSAGE = "BACK_TO_MAIN_FROM_SEND_MESSAGE";
    String GET_BALANCE = "GET_BALANCE";
    String MY_ORDERS = "MY_ORDERS";
    String SETTINGS = "SETTINGS";
    String NEW_ORDER_CLICKED = "NEW_ORDER_CLICKED";
    String OLD_ORDER_CLICKED = "OLD_ORDER_CLICKED";

}
