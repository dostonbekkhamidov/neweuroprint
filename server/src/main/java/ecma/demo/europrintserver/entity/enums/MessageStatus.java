package ecma.demo.europrintserver.entity.enums;

public enum  MessageStatus {

    ACTIVE,
    PRICING,
    DESIGNING,
    CONSULTING,
    ACCEPTED,
    IGNORED

}
