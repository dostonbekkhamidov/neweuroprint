package ecma.demo.europrintserver.entity.enums;

public enum OrderStatus {

    PENDING,
    IN_PROCESS,
    COMPLETED,
    ARCHIVE,
    IGNORED

}
