package ecma.demo.europrintserver.entity.enums;

public enum PayType {

    CASH,
    CARD,
    BANK

}
