package ecma.demo.europrintserver.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import ecma.demo.europrintserver.entity.enums.OrderPayStatus;
import ecma.demo.europrintserver.entity.enums.OrderStatus;
import ecma.demo.europrintserver.entity.template.AbsEntity;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "Orders")
public class Order extends AbsEntity {

    private String description;

    @Enumerated(EnumType.STRING)
    private OrderStatus orderStatus;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "order")
    @JsonBackReference
    private List<Product> products;

    @Enumerated(EnumType.STRING)
    private OrderPayStatus orderPayStatus;

    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    private Client client;

    @Column(nullable = false)
    private boolean isBot;

}
