package ecma.demo.europrintserver.entity;

import ecma.demo.europrintserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class IgnoredMessage extends AbsEntity {

    @Column(nullable = false)
    private String reason;

    @Column(nullable = false)
    private String text;

    @OneToOne(fetch = FetchType.LAZY)
    private Message message;

}
