package ecma.demo.europrintserver.entity;

import ecma.demo.europrintserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Master extends AbsEntity {

    @Column(nullable = false)
    private String fullName;

    private String description;

    @OneToOne(fetch = FetchType.LAZY)
    private Attachment attachment;

    private Boolean active;

}
