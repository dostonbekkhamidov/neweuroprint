package ecma.demo.europrintserver.entity;

import ecma.demo.europrintserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class TelegramState extends AbsEntity {

    private long tgId;

    private String tgState;

}
