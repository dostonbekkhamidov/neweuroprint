package ecma.demo.europrintserver.entity.enums;

public enum  RoleName {

    ROLE_DIRECTOR,
    ROLE_MANAGER,
    ROLE_ADMIN

}
