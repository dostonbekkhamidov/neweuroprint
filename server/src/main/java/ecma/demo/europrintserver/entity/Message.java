package ecma.demo.europrintserver.entity;

import ecma.demo.europrintserver.entity.enums.MessageStatus;
import ecma.demo.europrintserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import static javax.persistence.FetchType.LAZY;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "Message")
public class Message extends AbsEntity {

    @ManyToOne(fetch = LAZY)
    private Client client;

    @Column(nullable = false)
    private String text;

    @ManyToOne(fetch = LAZY)
    private User user;

    @ManyToOne(fetch = LAZY)
    private Order order;

    @Enumerated(EnumType.STRING)
    private MessageStatus messageStatus;

    private boolean isDisabled;

}
