package ecma.demo.europrintserver.entity;

import ecma.demo.europrintserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class MessageDefault extends AbsEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    private Client client;

    private String text;

}
