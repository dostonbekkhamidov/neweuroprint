package ecma.demo.europrintserver.entity;


import ecma.demo.europrintserver.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Portfolio extends AbsEntity {

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Attachment attachment;

}


