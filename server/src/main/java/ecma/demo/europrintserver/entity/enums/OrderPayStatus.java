package ecma.demo.europrintserver.entity.enums;

public enum  OrderPayStatus {

    NOT_PAID,
    PENDING,
    PAID

}
