package ecma.demo.europrintserver.service;

import ecma.demo.europrintserver.entity.*;
import ecma.demo.europrintserver.entity.enums.MessageStatus;
import ecma.demo.europrintserver.entity.enums.OrderPayStatus;
import ecma.demo.europrintserver.entity.enums.OrderStatus;
import ecma.demo.europrintserver.payload.ApiResponse;
import ecma.demo.europrintserver.payload.ReqOrder;
import ecma.demo.europrintserver.payload.ReqProduct;
import ecma.demo.europrintserver.payload.ResOrderByStatus;
import ecma.demo.europrintserver.repository.ClientRepository;
import ecma.demo.europrintserver.repository.MessageRepository;
import ecma.demo.europrintserver.repository.OrderRepository;
import ecma.demo.europrintserver.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class OrderService {

    @Autowired
    OrderRepository orderRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    ClientRepository clientRepository;
    @Autowired
    MessageRepository messageRepository;

    public HttpEntity<?> getAll() {

        List<Order> orders = orderRepository.findAll();
        return ResponseEntity.ok(new ApiResponse("success", true, orders));
    }

    public HttpEntity<?> getOne(UUID id) {
        List<Product> products = productRepository.getByOrderId(id);
        Message currentMassage = messageRepository.getCurrentMassage(id);
        return ResponseEntity.ok(new ApiResponse("success", true, products,currentMassage));
    }

    public HttpEntity<?> save(ReqOrder reqOrder) {

        if (reqOrder.getProducts().size() != 0) {
            Client client = clientRepository.findById(reqOrder.getClientId()).get();
            Order order = orderRepository.save(new Order(null, OrderStatus.PENDING, null, OrderPayStatus.NOT_PAID, client, reqOrder.isBot()));

            List<Product> products = new ArrayList<>();
            for (ReqProduct reqProduct : reqOrder.getProducts()) {
                Product product = new Product();
                product.setProductName(reqProduct.getProductName());
                product.setThreeDSize(reqProduct.getThreeDSize());
                product.setMaterial(reqProduct.getMaterial());
                product.setKnife(reqProduct.getKnife());
                product.setProcess(reqProduct.getProcess());
                product.setType(reqProduct.getType());
                product.setPrice(reqProduct.getPrice());
                product.setAmount(reqProduct.getAmount());
                product.setOrder(order);
                products.add(product);
            }
            productRepository.saveAll(products);

            if (reqOrder.getMessageId() != null) {
                Message message = messageRepository.findById(reqOrder.getMessageId()).get();
                message.setMessageStatus(MessageStatus.ACCEPTED);
                message.setOrder(order);
                messageRepository.save(message);
            }
            return ResponseEntity.ok(new ApiResponse("success", true));
        }
        return ResponseEntity.ok(new ApiResponse("products can't be empty", false));
    }

    public HttpEntity<?> updateOrder(ReqOrder reqOrder, UUID orderId) {
        try {
            List<Product> productList = productRepository.getByOrderId(orderId);
            Order order = orderRepository.findById(orderId).get();
            List<Product> products = new ArrayList<>();
            for (ReqProduct reqProduct : reqOrder.getProducts()) {
                Product product = new Product();
                product.setAmount(reqProduct.getAmount());
                product.setKnife(reqProduct.getKnife());
                product.setMaterial(reqProduct.getMaterial());
                product.setPrice(reqProduct.getPrice());
                product.setProcess(reqProduct.getProcess());
                product.setProductName(reqProduct.getProductName());
                product.setThreeDSize(reqProduct.getThreeDSize());
                product.setType(reqProduct.getType());
                product.setOrder(order);
                products.add(product);
            }
            productRepository.saveAll(products);
            order.setProducts(products);
            orderRepository.save(order);
            for (Product product:productList){
                productRepository.delete(product);
            }
            return ResponseEntity.ok(new ApiResponse("ok", true));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.ok(new ApiResponse("error", false));
        }
    }

    public HttpEntity<?> getOrders(String orderStatus, Integer page, Integer size) {
        List<Object[]> statuses = orderRepository.findAllByOrderStatus(page, size, orderStatus);
        List<ResOrderByStatus> orders = new ArrayList<>();
        for (Object[] status : statuses) {
            ResOrderByStatus order = new ResOrderByStatus();
            order.setId(UUID.fromString(status[0].toString()));
            order.setOrderPayStatus(status[1] != null ? status[1].toString() : null);
            order.setCreatedAt(status[2] != null ? (status[2].toString()) : null);
            order.setSum(status[3] != null ? Double.parseDouble(status[3].toString()) : null);
            order.setClientId(UUID.fromString(status[4].toString()));
            order.setFullName(status[5] != null ? status[5].toString() : null);
            order.setPhoneNumber(status[6] != null ? status[6].toString() : null);
            orders.add(order);
        }
        Integer totalElements = orderRepository.orderTotalElements(orderStatus);
        return ResponseEntity.ok(new ApiResponse("success", true, orders,totalElements));
    }


    public HttpEntity<?> updateOrderStatus(UUID id, String status) {
        Order order = orderRepository.findById(id).get();
        order.setOrderStatus(OrderStatus.valueOf(status));
        orderRepository.save(order);
        return ResponseEntity.ok(new ApiResponse("success", true));
    }

    public HttpEntity<?> getReportTopPage() {
        return ResponseEntity.ok(new ApiResponse("success", true, orderRepository.reportTopPage()));
    }

    public HttpEntity<?> getReportBelowPage(String month) {
        return ResponseEntity.ok(new ApiResponse("success", true, orderRepository.reportBelowPage(month)));
    }
}
