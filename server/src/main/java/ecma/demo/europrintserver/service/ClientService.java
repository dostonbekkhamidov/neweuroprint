package ecma.demo.europrintserver.service;

import ecma.demo.europrintserver.entity.Client;
import ecma.demo.europrintserver.payload.ApiResponse;
import ecma.demo.europrintserver.payload.ReqClient;
import ecma.demo.europrintserver.payload.ResClient;
import ecma.demo.europrintserver.repository.ClientRepository;
import ecma.demo.europrintserver.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class ClientService {

    @Autowired
    ClientRepository clientRepository;

    public HttpEntity<?> get(String search, Integer page,Integer size) {
        List<Object[]> clients = clientRepository.getAllClientsWithBalance(page - 1, size, search);
        List<ResClient> resClients = new ArrayList<>();
        for (Object[] client : clients) {
            ResClient resClient = new ResClient();
            resClient.setUuid(UUID.fromString(client[0].toString()));
            resClient.setFullName(client[1] != null ? client[1].toString() : null);
            resClient.setPhoneNumber(client[2].toString());
            resClient.setTgId(client[3] != null ? Long.parseLong(client[3].toString()) : null);
            resClient.setClientBalance(Double.parseDouble(client[4].toString()));
            resClients.add(resClient);
        }

        Integer totalElements = clientRepository.getAllClientsTotalElements();

        return ResponseEntity.ok(new ApiResponse("success", true, resClients, totalElements));
    }

    public HttpEntity<?> getHistoryByManager(UUID id) {
        return ResponseEntity.ok(new ApiResponse("success", true));
    }

    public HttpEntity<?> getClientBalance(UUID id) {
        UUID uuid = clientRepository.client(id);
        Double clientBalance = clientRepository.clientBalance(uuid);
        return ResponseEntity.ok(new ApiResponse("success",true,clientBalance));
    }

    public HttpEntity<?> save(ReqClient reqClient) {
        try {
            clientRepository.save(new Client(reqClient.getFullName(), reqClient.getPhoneNumber()));
            return ResponseEntity.ok(new ApiResponse("success", true));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.ok(new ApiResponse("error", false));
        }
    }


    public HttpEntity<?> edit(UUID id, ReqClient reqClient) {
        try {
            Client client = clientRepository.findById(id).get();
            client.setFullName(reqClient.getFullName());
            client.setPhoneNumber(reqClient.getPhoneNumber());
            clientRepository.save(client);
            return ResponseEntity.ok(new ApiResponse("success", true));
        } catch (Exception e) {
            return ResponseEntity.ok(new ApiResponse("error", false));
        }
    }

    public HttpEntity<?> delete(UUID id) {
        try {
            clientRepository.deleteById(id);
            return ResponseEntity.ok(new ApiResponse("success", true));
        } catch (Exception e) {
            return ResponseEntity.ok(new ApiResponse("error", false));
        }
    }

}
