package ecma.demo.europrintserver.service;

import ecma.demo.europrintserver.entity.Master;
import ecma.demo.europrintserver.exception.ResourceNotFoundException;
import ecma.demo.europrintserver.payload.ApiResponse;
import ecma.demo.europrintserver.payload.ReqEditMAster;
import ecma.demo.europrintserver.repository.AttachmentContentRepository;
import ecma.demo.europrintserver.repository.AttachmentRepository;
import ecma.demo.europrintserver.repository.MasterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class MasterService {
    @Autowired
    MasterRepository masterRepository;
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    AttachmentService attachmentService;

    public ApiResponse editingMaster(ReqEditMAster reqEditMAster) {
        Master master = masterRepository.findById(reqEditMAster.getId()).orElseThrow(() -> new ResourceNotFoundException("s", "s", reqEditMAster.getId()));
        master.setActive(reqEditMAster.getActive());
        master.setAttachment(attachmentRepository.findById(reqEditMAster.getAttachment()).orElseThrow(() ->  new ResourceNotFoundException("s","s",reqEditMAster.getAttachment())));
        master.setFullName(reqEditMAster.getName());
        master.setDescription(reqEditMAster.getDescription());
        masterRepository.save(master);
        return new ApiResponse("Edited",true);
    }

    public HttpEntity<?> delete(UUID id) {
        try {
            Master master = masterRepository.findById(id).get();
            UUID uuid = master.getAttachment().getId();
            masterRepository.deleteById(id);
            attachmentService.delete(uuid);
            return ResponseEntity.ok(new ApiResponse("success", true));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.ok(new ApiResponse("error", false));
        }
    }
}
