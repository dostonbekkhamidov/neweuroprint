package ecma.demo.europrintserver.service;

import ecma.demo.europrintserver.entity.Client;
import ecma.demo.europrintserver.entity.Order;
import ecma.demo.europrintserver.entity.OrderPayment;
import ecma.demo.europrintserver.entity.Payment;
import ecma.demo.europrintserver.entity.enums.OrderPayStatus;
import ecma.demo.europrintserver.payload.ApiResponse;
import ecma.demo.europrintserver.payload.ReqPayment;
import ecma.demo.europrintserver.repository.*;
import org.omg.CORBA.INTERNAL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class PaymentService {

    @Autowired
    PaymentRepository paymentRepository;
    @Autowired
    ClientRepository clientRepository;
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    OrderPaymentRepository orderPaymentRepository;



    public HttpEntity<?> get(UUID clientId, Integer page, Integer size) {
        Pageable pageable= PageRequest.of(page-1,size);
        Page<Payment> paymentsByClientId = paymentRepository.findAllByClientIdOrderByCreatedAtDesc(pageable, clientId);
        return ResponseEntity.ok(new ApiResponse("success",true,paymentsByClientId));
    }

    public HttpEntity<?> save(ReqPayment reqPayment) {

        Client client = clientRepository.findById(reqPayment.getClientId()).get();
        paymentRepository.save(new Payment(reqPayment.getAmount(),reqPayment.getDescription(),reqPayment.getPayType(),client));
        List<Order> orderList = orderRepository.getNotPaidOrders(reqPayment.getClientId());
        for (Order order : orderList) {
            Double paymentForPendingOrder=orderRepository.getPaidSumForOrder(order.getId());
            if (reqPayment.getAmount()>=paymentForPendingOrder){
                reqPayment.setAmount(reqPayment.getAmount()-paymentForPendingOrder);
                orderPaymentRepository.save(new OrderPayment(paymentForPendingOrder,order));
                order.setOrderPayStatus(OrderPayStatus.PAID);
            }else {
                orderPaymentRepository.save(new OrderPayment(reqPayment.getAmount(),order));
                order.setOrderPayStatus(OrderPayStatus.PENDING);
                break;
            }
        }
        orderRepository.saveAll(orderList);
        return ResponseEntity.ok(new ApiResponse("success",true));
    }
}
