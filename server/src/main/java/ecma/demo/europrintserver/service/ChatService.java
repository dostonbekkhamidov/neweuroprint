package ecma.demo.europrintserver.service;

import ecma.demo.europrintserver.bot.MyBot;
import ecma.demo.europrintserver.entity.*;
import ecma.demo.europrintserver.payload.ApiResponse;
import ecma.demo.europrintserver.payload.ReqChat;
import ecma.demo.europrintserver.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.UUID;

@Service
public class ChatService {

    @Autowired
    ChatRepository chatRepository;
    @Autowired
    MessageRepository messageRepository;
    @Autowired
    MyBot myBot;
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    AttachmentContentRepository attachmentContentRepository;
    @Autowired
    TgMessageRepository tgMessageRepository;

    public HttpEntity<?> getAll(UUID messageId) {
        return null;
    }

    public HttpEntity<?> saveChatWithText(ReqChat reqChat) {
        Message message = messageRepository.findById(reqChat.getMessageId()).get();
        String text = "<b>Xabar: </b> " + reqChat.getText() + "\nMurojaat uchun: " + message.getUser().getPhoneNumber();
        chatRepository.save(new Chat(reqChat.getText(), null, message, message.getUser()));
        myBot.send(new SendMessage(message.getClient().getTgId(), text));
        return ResponseEntity.ok(new ApiResponse("success", true));
    }

    public HttpEntity<?> saveChatWithFile(MultipartHttpServletRequest request) throws IOException, TelegramApiException {
        Message message = messageRepository.findById(UUID.fromString(request.getParameter("messageId"))).get();
        MultipartFile multipartFile = request.getFile(request.getFileNames().next());
        Attachment attachment = attachmentRepository.save(new Attachment(multipartFile.getSize(), multipartFile.getName(), multipartFile.getOriginalFilename(), multipartFile.getContentType()));
        AttachmentContent attachmentContent = attachmentContentRepository.save(new AttachmentContent(multipartFile.getBytes(), attachment));
        chatRepository.save(new Chat("", attachment, message, message.getUser()));
        SendDocument sendDocument = new SendDocument();
        sendDocument.setChatId(message.getClient().getTgId());
        sendDocument.setParseMode(ParseMode.HTML);
        sendDocument.setCaption("Salom");
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(attachmentContent.getContent());
        sendDocument.setDocument(new InputFile(byteArrayInputStream,String.valueOf(byteArrayInputStream)));
        org.telegram.telegrambots.meta.api.objects.Message execute = myBot.execute(sendDocument);
        tgMessageRepository.save(new TgMessage(execute.getChatId(),execute.getMessageId()));
        return ResponseEntity.ok(new ApiResponse("success",true));
    }
}
