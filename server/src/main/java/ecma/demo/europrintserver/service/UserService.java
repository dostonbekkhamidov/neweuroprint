package ecma.demo.europrintserver.service;

import ecma.demo.europrintserver.entity.IgnoredMessage;
import ecma.demo.europrintserver.entity.User;
import ecma.demo.europrintserver.entity.enums.RoleName;
import ecma.demo.europrintserver.payload.ApiResponse;
import ecma.demo.europrintserver.payload.ReqUser;
import ecma.demo.europrintserver.payload.ResUserByMassage;
import ecma.demo.europrintserver.repository.IgnoredMessageRepository;
import ecma.demo.europrintserver.repository.RoleRepository;
import ecma.demo.europrintserver.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
public class UserService {


    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    IgnoredMessageRepository ignoredMessageRepository;

    public HttpEntity<?> getMe(User user) {
        try {
            return ResponseEntity.ok(new ApiResponse("success", true, user));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.ok(new ApiResponse("error", false, null));
        }
    }


    public HttpEntity<?> getAllUsersByMessage() {
        List<Object[]> objects = userRepository.getAllByUser();
        List<ResUserByMassage> users = new ArrayList<>();
        for (Object[] object : objects) {
            ResUserByMassage user = new ResUserByMassage();
            user.setFullName(object[0] != null ? object[0].toString() : null);
            user.setUserId(object[1] != null ? UUID.fromString(object[1].toString()) : null);
            user.setTotal(Integer.parseInt(object[2].toString()));
            user.setAccepted(Integer.parseInt(object[3].toString()));
            user.setPricing(Integer.parseInt(object[4].toString()));
            user.setDesigning(Integer.parseInt(object[5].toString()));
            user.setConsulting(Integer.parseInt(object[6].toString()));
            user.setIgnored(Integer.parseInt(object[7].toString()));
            users.add(user);
        }
        return ResponseEntity.ok(new ApiResponse("success", true, users));
    }

    public HttpEntity<?> getAllByIgnored(UUID id) {
        List<IgnoredMessage> ignoredMessages = ignoredMessageRepository.getAllByManager(id);
        return ResponseEntity.ok(new ApiResponse("success", true, ignoredMessages));
    }

    public HttpEntity<?> getOne(UUID id) {
        User user = userRepository.findById(id).get();
        return ResponseEntity.ok(new ApiResponse("success", true, user));
    }

    public HttpEntity<?> saveUser(ReqUser user) {
        try {
            User save = userRepository.save(new User(
                    user.getFirstName(),
                    user.getLastName(),
                    user.getPhoneNumber(),
                    user.getPassportSerial(),
                    user.getPassportNumber(),
                    user.getUsername(),
                    passwordEncoder.encode(user.getPassword()),
                    roleRepository.findAllByRoleName(RoleName.ROLE_MANAGER)
            ));
            return ResponseEntity.ok(new ApiResponse("ok", true, save));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.ok(new ApiResponse("error", false, null));

        }
    }

    public HttpEntity<?> updateUser(ReqUser reqUser, UUID id) {
        User user = userRepository.findById(id).get();

        user.setPassword(passwordEncoder.encode(reqUser.getPassword()));
        user.setPassportSerial(reqUser.getPassportSerial());
        user.setPassportNumber(reqUser.getPassportNumber());
        user.setLastName(reqUser.getLastName());
        user.setFirstName(reqUser.getFirstName());
        user.setPhoneNumber(reqUser.getPhoneNumber());
        user.setUsername(reqUser.getUsername());
        userRepository.save(user);
        return ResponseEntity.ok(new ApiResponse("ok", true));
    }

    public HttpEntity<?> checkUserPassword(UUID userId, String password) {
        User user = userRepository.findById(userId).get();
        String encode = passwordEncoder.encode(password);
        if (encode.equalsIgnoreCase(user.getPassword())) {
            return ResponseEntity.ok(new ApiResponse("ok", true));
        }
        return ResponseEntity.ok(new ApiResponse("password is not ........", false));

//        return passwordEncoder.encode(password).equals(user.getPassword()) ? ResponseEntity.ok(new ApiResponse("ok",true)) : ResponseEntity.ok(new ApiResponse("password is not ........",false)) ;
    }
}
