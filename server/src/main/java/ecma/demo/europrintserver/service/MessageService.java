package ecma.demo.europrintserver.service;

import ecma.demo.europrintserver.entity.Client;
import ecma.demo.europrintserver.entity.Message;
import ecma.demo.europrintserver.entity.Order;
import ecma.demo.europrintserver.entity.User;
import ecma.demo.europrintserver.entity.enums.MessageStatus;
import ecma.demo.europrintserver.payload.ApiResponse;
import ecma.demo.europrintserver.repository.ClientRepository;
import ecma.demo.europrintserver.repository.MessageRepository;
import ecma.demo.europrintserver.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MessageService {

    @Autowired
    ClientRepository clientRepository;
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    MessageRepository messageRepository;
    @Autowired
    SimpMessageSendingOperations simpMessageSendingOperations;

    public HttpEntity<?> getAll(User user) {
        List<Message> myCurrentMessage = messageRepository.findAllByUserIdAndIsDisabled(user.getId(), true);
        return ResponseEntity.ok(new ApiResponse(
                "success",
                true,
                messageRepository.findAllByMessageStatusOrderByCreatedAtAsc(MessageStatus.ACTIVE),
                myCurrentMessage.size() == 0 ? null : myCurrentMessage.get(0)));
    }

    public Message generateMessageForNewOrder(String description, Long chatId) {
        Optional<Client> client = clientRepository.findByTgId(chatId);
        if (client.isPresent()) {
            Message message = messageRepository.save(new Message(
                    client.get(),
                    description,
                    null,
                    null,
                    MessageStatus.ACTIVE,
                    false
            ));
            simpMessageSendingOperations.convertAndSend("/chat/all", message);
            return message;
        }
        return null;
    }

    public Message generateMessageForOldOrder(String description, Long chatId, UUID orderId) {
        Optional<Client> client = clientRepository.findByTgId(chatId);
        if (client.isPresent()) {
            Message message = messageRepository.save(new Message(
                    client.get(),
                    description,
                    null,
                    orderRepository.findById(orderId).get(),
                    MessageStatus.ACTIVE,
                    false
            ));
            simpMessageSendingOperations.convertAndSend("/chat/all", message);
            return message;
        }
        return null;
    }

    public HttpEntity<?> update(UUID id, String state, User user) {
        Message message = messageRepository.findById(id).get();
        message.setMessageStatus(MessageStatus.valueOf(state));
        if (state.equalsIgnoreCase("Active")) {
            message.setUser(null);
        } else {
            message.setUser(user);
        }
        Message save = messageRepository.save(message);
        simpMessageSendingOperations.convertAndSend("/chat/changeStatus", save);
        return ResponseEntity.ok(new ApiResponse("success", true));
    }

    public HttpEntity<?> changeDisabled(UUID id, User user) {
        Message message = messageRepository.findById(id).get();
        if (message.getUser() == null) {
            message.setDisabled(true);
            message.setUser(user);
        } else {
            message.setDisabled(false);
            message.setUser(null);
        }
        Message save = messageRepository.save(message);
        simpMessageSendingOperations.convertAndSend("/chat/changeStatus", save);
        return ResponseEntity.ok(new ApiResponse("success", true));
    }

    public HttpEntity<?> getAllMessagesByStatus(User user, Integer size, MessageStatus messageStatus) {
        Pageable pageable = PageRequest.of(0, size);
        return ResponseEntity.ok(new ApiResponse(
                "success",
                true,
                !String.valueOf(messageStatus).equals("ACTIVE") ?
                messageRepository.findAllByUserIdAndMessageStatusOrderByCreatedAtDesc(pageable, user.getId(), messageStatus)
                :messageRepository.findAllByMessageStatusOrderByCreatedAtDesc(pageable,messageStatus)));
    }

    public HttpEntity<?> updateStatus(UUID id, String status, User user) {
        Message message = messageRepository.findById(id).get();
        message.setMessageStatus(MessageStatus.valueOf(status));
        message.setUser(user);
        message.setDisabled(false);
        Message save = messageRepository.save(message);
        return ResponseEntity.ok(new ApiResponse("success", true));
    }

}
