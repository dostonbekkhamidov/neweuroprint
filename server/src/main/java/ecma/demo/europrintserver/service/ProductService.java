package ecma.demo.europrintserver.service;

import ecma.demo.europrintserver.entity.Product;
import ecma.demo.europrintserver.payload.ApiResponse;
import ecma.demo.europrintserver.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;


    public HttpEntity<?> getOrderSum(UUID orderId) {
        Double aDouble = productRepository.findByOrderId(orderId);
        return ResponseEntity.ok(new ApiResponse("success",true,aDouble));
    }


    public HttpEntity<?> getProductList(UUID uuid) {
        List<Product> productList = productRepository.getByOrderId(uuid);

        return ResponseEntity.ok(new ApiResponse("success",true,productList));
    }
}
