package ecma.demo.europrintserver.payload;

import ecma.demo.europrintserver.entity.Master;
import ecma.demo.europrintserver.entity.Portfolio;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HomePageResponse {
    private List<Portfolio> portfolios;
    private List<Master> masters;
}
