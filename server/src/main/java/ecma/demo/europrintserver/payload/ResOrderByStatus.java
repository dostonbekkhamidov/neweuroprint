package ecma.demo.europrintserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResOrderByStatus {

    private UUID id;
    private String orderPayStatus;
    private String createdAt;
    private Double sum;
    private UUID clientId;
    private String fullName;
    private String phoneNumber;

}
