package ecma.demo.europrintserver.payload;

import ecma.demo.europrintserver.entity.Message;
import ecma.demo.europrintserver.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.awt.print.Pageable;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiResponse {

    private String message;
    private Boolean success;
    private Object object;
    private Integer totalElements;
    private Message currentMessage;
    private Integer productAmount;
    private Double orderPayment;
    private Double payment;
    private Double paymentBack;

    public ApiResponse(String message, Boolean success) {
        this.message = message;
        this.success = success;
    }

    public ApiResponse(String message, Boolean success, Object object) {
        this.message = message;
        this.success = success;
        this.object = object;
    }

    public ApiResponse(String message, Boolean success, Object object, Message currentMessage) {
        this.message = message;
        this.success = success;
        this.object = object;
        this.currentMessage = currentMessage;
    }

    public ApiResponse(String message, Boolean success, Object object, Integer totalElements) {
        this.message = message;
        this.success = success;
        this.object = object;
        this.totalElements = totalElements;
    }

    public ApiResponse(String message, Boolean success, Object object, Integer totalElements, Integer productAmount, Double orderPayment, Double payment, Double paymentBack) {
        this.message = message;
        this.success = success;
        this.object = object;
        this.totalElements = totalElements;
        this.productAmount = productAmount;
        this.orderPayment = orderPayment;
        this.payment = payment;
        this.paymentBack = paymentBack;
    }
}
