package ecma.demo.europrintserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResUserByMassage {

    private String fullName;
    private UUID userId;
    private Integer total;
    private Integer accepted;
    private Integer pricing;
    private Integer designing;
    private Integer consulting;
    private Integer ignored;

}
