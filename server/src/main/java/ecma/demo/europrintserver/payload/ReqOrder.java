package ecma.demo.europrintserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqOrder {

    private List<ReqProduct> products;

    private boolean isBot;

    private UUID clientId;

    private UUID messageId;

}
