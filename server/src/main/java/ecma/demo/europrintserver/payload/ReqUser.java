package ecma.demo.europrintserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqUser {

    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String passportSerial;
    private String passportNumber;
    private String username;
    private String password;

}
