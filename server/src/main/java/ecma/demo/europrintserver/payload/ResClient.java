package ecma.demo.europrintserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResClient {

    private UUID uuid;
    private String fullName;
    private String phoneNumber;
    private Long tgId;
    private Double clientBalance;

}
