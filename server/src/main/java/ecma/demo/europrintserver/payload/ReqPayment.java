package ecma.demo.europrintserver.payload;

import ecma.demo.europrintserver.entity.enums.PayType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqPayment {

    private UUID clientId;

    private Double amount;

    private PayType payType;

    private String description;

}
