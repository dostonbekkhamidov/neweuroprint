package ecma.demo.europrintserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResReconciliation {

    private UUID id;
    private String createdAt;
    private String process;
    private String name;
    private Integer amount;
    private Double price;
    private Double payment;

}
