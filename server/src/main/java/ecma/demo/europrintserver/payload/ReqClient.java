package ecma.demo.europrintserver.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqClient {

    private String fullName;

    private String phoneNumber;

}
