package ecma.demo.europrintserver.config;

import ecma.demo.europrintserver.entity.Company;
import ecma.demo.europrintserver.entity.Role;
import ecma.demo.europrintserver.entity.User;
import ecma.demo.europrintserver.entity.enums.RoleName;
import ecma.demo.europrintserver.repository.CompanyRepository;
import ecma.demo.europrintserver.repository.RoleRepository;
import ecma.demo.europrintserver.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;

@Component
public class DataLoader implements CommandLineRunner {

    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    CompanyRepository companyRepository;

    @Value("${spring.datasource.initialization-mode}")
    private String initialMode;

    @Override
    public void run(String... args) throws Exception {
        if (initialMode.equals("always")) {

            companyRepository.save(new Company(
                    1,
                    "O'zbekiston R,Farg'ona viloyati, Qo'qon sh.Usta bozor k,1B uy.",
                    "O'zbekiston R,Farg'ona viloyati, Qo'qon sh.Usta bozor k,1B uy.",
                    "info@example.com",
                    "https://www.facebook.com/europrintkokand/",
                    "https://www.instagram.com/europrint.official/",
                    "https://www.youtube.com/channel/UCb0MkCvYXQ2acw842h0TN4g",
                    "https://t.me/Europrintuz",
                    "+998912041100",
                    "+998903607700",
                    "+998902831100",
                    1200,
                    24000,
                    250
            ));

            userRepository.save(
                    new User(
                            "Dadaxon",
                            "Pozilov",
                            "+998903607700",
                            "AB",
                            "3558262",
                            "998903607700",
                            passwordEncoder.encode("admin1950"),
                            roleRepository.saveAll(
                                    new ArrayList<>(Arrays.asList(
                                            new Role(1, RoleName.ROLE_ADMIN, "admin"),
                                            new Role(2, RoleName.ROLE_DIRECTOR, "director"),
                                            new Role(3, RoleName.ROLE_MANAGER, "manager"))
                                    )))

            );
        }
    }
}
