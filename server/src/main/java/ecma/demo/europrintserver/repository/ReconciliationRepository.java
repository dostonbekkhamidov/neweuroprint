package ecma.demo.europrintserver.repository;

import ecma.demo.europrintserver.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface ReconciliationRepository extends JpaRepository<Client, UUID> {

    @Query(value = "select * from ((select cast(p2.order_uuid as varchar(50)), to_char(p2.created_at, 'YYYY-MM-DD HH:mm') as date, " +
            "'Order' as process, p2.product_name as name, coalesce(p2.amount,0) as amount, coalesce(p2.price,0) as price, null as payment " +
            "from product p2 where p2.order_uuid in (select o.uuid from orders o where o.client_uuid= :clientId)) " +
            "union all" +
            "(select cast(p.uuid as varchar(50)), to_char(p.created_at, 'YYYY-MM-DD HH:mm') as date, 'Payment' as process, " +
            "null as name, null as amount, null as price, p.amount as payment from payment p where p.client_uuid= :clientId) " +
            "union all" +
            "(select cast(pb.uuid as varchar(50)), to_char(pb.created_at, 'YYYY-MM-DD HH:mm') as date, 'Return' as process, " +
            "null as name, null as amount, null as price, pb.amount as payment from payment_back pb " +
            "where pb.client_uuid= :clientId)) " +
            "results order by date limit :size offset :size * :page", nativeQuery = true)
    List<Object[]> getClientTotalReport(UUID clientId, Integer size, Integer page);

    @Query(value = "select count(*) from (" +
            "(select p2.amount from product p2 where p2.order_uuid in (select o.uuid from orders o where o.client_uuid=:clientId))" +
            "union all (select p.amount from payment p where p.client_uuid=:clientId) " +
            "union all (select pb.amount from payment_back pb where pb.client_uuid=:clientId)" +
            ") results", nativeQuery = true)
    Integer getClientReportTotalElements(UUID clientId);

    @Query(value = "select * from client c where c.full_name like concat('%',:search,'%') " +
            "or c.phone_number like concat('%',:search,'%') limit :size offset :size*:page", nativeQuery = true)
    List<Client> getAllClients(Integer page, Integer size, String search);

    @Query(value = "select count(*) from client", nativeQuery = true)
    Integer getAllClientsTotalElements();

    @Query(value = "select sum(p.amount) from payment p where p.client_uuid=:clientId", nativeQuery = true)
    Double getPayment(UUID clientId);

    @Query(value = "select coalesce(sum(p.amount*p.price),0)from product p where p.order_uuid in (select o.uuid from orders o where o.client_uuid=:clientId)", nativeQuery = true)
    Double getOrderPayment(UUID clientId);

    @Query(value = "select coalesce(sum(p.amount),0) from product p where p.order_uuid in " +
            "(select o.uuid from orders o where o.client_uuid=:clientId)", nativeQuery = true)
    Integer getProductAmount(UUID clientId);

    @Query(value = "select coalesce(sum(pb.amount),0) from payment_back pb where pb.client_uuid=:clientId", nativeQuery = true)
    Double getPaymentBack(UUID clientId);

}
