package ecma.demo.europrintserver.repository;

import ecma.demo.europrintserver.entity.Payment;
import ecma.demo.europrintserver.entity.PaymentBack;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PaymentBackRepository extends JpaRepository<PaymentBack, UUID> {
}
