package ecma.demo.europrintserver.repository;

import ecma.demo.europrintserver.entity.Role;
import ecma.demo.europrintserver.entity.enums.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {

    List<Role> findAllByRoleName(RoleName roleName);
}
