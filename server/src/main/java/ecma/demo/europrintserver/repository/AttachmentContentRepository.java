package ecma.demo.europrintserver.repository;

import ecma.demo.europrintserver.entity.Attachment;
import ecma.demo.europrintserver.entity.AttachmentContent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.UUID;

public interface AttachmentContentRepository extends JpaRepository<AttachmentContent, UUID> {

    AttachmentContent getByAttachment(Attachment attachment);

    @Modifying
    @Query(value = "delete from attachment_content ac where ac.attachment_id = :uuid", nativeQuery = true)
    void deleteByAttachmentId(UUID uuid);
}
