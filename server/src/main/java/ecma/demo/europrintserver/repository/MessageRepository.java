package ecma.demo.europrintserver.repository;

import ecma.demo.europrintserver.entity.Message;
import ecma.demo.europrintserver.entity.enums.MessageStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface MessageRepository extends JpaRepository<Message, UUID> {

    List<Message> findAllByMessageStatusOrderByCreatedAtAsc(MessageStatus messageStatus);

    Page<Message> findAllByUserIdAndMessageStatusOrderByCreatedAtDesc(Pageable pageable, UUID userId, MessageStatus messageStatus);

    Page<Message> findAllByMessageStatusOrderByCreatedAtDesc(Pageable pageable, MessageStatus messageStatus);

    @Query(value = "select * from message m where m.message_status !='ACCEPTED' and m.is_disabled=:isDisabled " +
            "and m.user_uuid=:userId", nativeQuery = true)
    List<Message> findAllByUserIdAndIsDisabled(UUID userId, boolean isDisabled);

    @Query(value = "select * from message m where m.order_uuid =:orderId", nativeQuery = true)
    Message getCurrentMassage(UUID orderId);

}
