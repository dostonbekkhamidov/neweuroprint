package ecma.demo.europrintserver.repository;

import ecma.demo.europrintserver.entity.Company;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CompanyRepository extends JpaRepository<Company, Integer> {

    Optional<Company> findById(Integer id);

}
