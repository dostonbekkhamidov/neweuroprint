package ecma.demo.europrintserver.repository;

import ecma.demo.europrintserver.entity.IgnoredMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface IgnoredMessageRepository extends JpaRepository<IgnoredMessage, UUID> {

    @Query(value = "select * from ignored_message im " +
            "where im.message_uuid in " +
            "(select m.uuid from message m where m.user_uuid=:id) order by im.created_at"
            ,nativeQuery = true)
    List<IgnoredMessage> getAllByManager(UUID id);

}
