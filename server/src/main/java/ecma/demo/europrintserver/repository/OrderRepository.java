package ecma.demo.europrintserver.repository;

import ecma.demo.europrintserver.entity.Order;
import ecma.demo.europrintserver.entity.enums.OrderStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface OrderRepository extends JpaRepository<Order, UUID> {

    Page<Order> findAllByClientIdOrderByCreatedAtDesc(Pageable pageable, UUID clientId);

    @Query(value = "select cast(o.uuid as varchar(50)) as orderId, " +
            "order_pay_status, " +
            "to_char(created_at, 'DD/MM/YYYY  HH:mm'), " +
            "(select coalesce(sum(price*amount),0) from product where product.order_uuid= o.uuid), " +
            "cast((select client.uuid from client where client.uuid=o.client_uuid) as varchar(50)) as clientId, " +
            "(select full_name from client where client.uuid=o.client_uuid), "+
            "(select phone_number from client where client.uuid=o.client_uuid) "+
            "from orders o  where order_status = :orderStatus " +
            "order by created_at limit :size offset :page * :size", nativeQuery = true)
    List<Object[]> findAllByOrderStatus(Integer page, Integer size, String orderStatus);

    @Query(value = "select coalesce(count(*),0) from orders o where o.order_status=:orderStatus",nativeQuery = true)
    Integer orderTotalElements(String orderStatus);

    @Query(value = "select uuid,order_pay_status, (select sum(price*amount) from product where product.order_uuid = orders.uuid) from orders;",nativeQuery = true)
    Order get();

    @Query(value = " select * from orders where client_uuid=:clientId and order_pay_status <> 'PAID' and order_status in ('COMPLETED','IN_PROCESS') ORDER BY created_at ASC ", nativeQuery = true)
    List<Order> getNotPaidOrders(UUID clientId);

    @Query(value = "select (coalesce((select sum(amount*price) from product where order_uuid=:orderId),0)-" +
            "coalesce((select sum(amount) from order_payment where order_uuid=:orderId),0))", nativeQuery = true)
    Double getPaidSumForOrder(UUID orderId);

    @Query(value = "select" +
            "(select ((select coalesce(sum(amount), 0) from payment)-" +
            "(select coalesce(sum(amount), 0) from payment_back)-" +
            "(select coalesce(sum(amount), 0) from order_payment op)) as client_sum)," +
            "" +
            "(select (select coalesce(sum(amount*price), 0) from product p join orders o on o.uuid=p.order_uuid)-" +
            "(select coalesce(sum(amount), 0) from order_payment op) as company_sum)," +
            "" +
            "(select count(*) as proccess from orders o where o.order_status='IN_PROCESS')", nativeQuery = true)
    Object reportTopPage();

    @Query(value = "select" +
            "(select count(*) from orders o where (select to_char(o.created_at, 'yyyy-mm'))=?1 and order_status='COMPLETED') as completed," +
            "(select count(*) from orders o where (select to_char(o.created_at, 'yyyy-mm'))=?1 and order_status='PENDING') as pending", nativeQuery = true)
    Object reportBelowPage(String month);
}
