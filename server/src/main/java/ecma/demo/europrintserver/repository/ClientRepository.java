package ecma.demo.europrintserver.repository;

import ecma.demo.europrintserver.entity.Client;
import ecma.demo.europrintserver.entity.Order;
import ecma.demo.europrintserver.payload.ResClient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ClientRepository extends JpaRepository<Client, UUID> {

    Optional<Client> findByTgId(Long telegramId);

    @Query(value = "select cast(c.uuid as varchar), c.full_name, c.phone_number, c.tg_id, " +
            "(coalesce((select sum(amount) from payment p2  where p2.client_uuid = c.uuid), 0) - " +
            "coalesce((select sum(p.amount * p.price) from orders o join product p on o.uuid = p.order_uuid " +
            "where o.client_uuid = c.uuid and o.order_status != 'IGNORED'), 0)) as current_calance " +
            "from client c " +
            "where lower (c.full_name) like '%'||lower(:search)||'%' or c.phone_number like '%'||:search||'%' " +
            "limit :size offset :page*:size", nativeQuery = true)
    List<Object[]> getAllClientsWithBalance(Integer page, Integer size, String search);

    @Query(value = "select count(*) from client ", nativeQuery = true)
    Integer getAllClientsTotalElements();

    @Query(value = "select (coalesce((select sum(amount) from payment p2  where p2.client_uuid = :clientId), 0) - " +
            "coalesce((select sum(p.amount * p.price) from orders o join product p on o.uuid = p.order_uuid " +
            "where o.client_uuid = :clientId and o.order_status in ('IN_PROCESS','COMPLETED','ARCHIVE')), 0)) from res",nativeQuery = true)
    Double clientBalance(UUID clientId);

    @Query(value = "select cast(o.client_uuid as varchar(50)) from orders o where o.uuid=:id", nativeQuery = true)
    UUID client(UUID id);

}
