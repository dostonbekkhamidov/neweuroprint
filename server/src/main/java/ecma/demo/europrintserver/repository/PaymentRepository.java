package ecma.demo.europrintserver.repository;

import ecma.demo.europrintserver.entity.Payment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PaymentRepository extends JpaRepository<Payment, UUID> {

    Page<Payment> findAllByClientIdOrderByCreatedAtDesc(Pageable pageable,UUID clientId);

}
