package ecma.demo.europrintserver.repository;

import ecma.demo.europrintserver.entity.Attachment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.UUID;

public interface AttachmentRepository extends JpaRepository<Attachment, UUID> {

    @Modifying
    @Query(value = "delete FROM attachment a WHERE a.uuid=:uuid", nativeQuery = true)
    void deleteById(UUID uuid);

}
