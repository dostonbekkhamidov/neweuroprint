package ecma.demo.europrintserver.repository;

import ecma.demo.europrintserver.entity.Order;
import ecma.demo.europrintserver.entity.OrderPayment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.UUID;

public interface OrderPaymentRepository extends JpaRepository<OrderPayment, UUID> {

    @Query(value = "select coalesce(sum(amount),0) from order_payment where order_uuid=:orderId",nativeQuery = true)
    Double findByOrderId(UUID orderId);


}
