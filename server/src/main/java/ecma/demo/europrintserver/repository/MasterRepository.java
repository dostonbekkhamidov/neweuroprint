package ecma.demo.europrintserver.repository;

import ecma.demo.europrintserver.entity.Master;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface MasterRepository extends JpaRepository<Master, UUID> {

    @Query(value = "select * from master order by created_at asc",nativeQuery = true)
    List<Master> findAllByCreatedAtAsc();

}
