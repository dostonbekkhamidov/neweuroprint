package ecma.demo.europrintserver.repository;

import ecma.demo.europrintserver.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    Optional<User> findByUsername(String username);

    @Query(value = "select concat(u.first_name,' ', u.last_name) as fullName, " +
            "cast(u.uuid as varchar(50)), " +
            "sum(case when m.user_uuid=u.uuid then 1 else  0 end) as total, " +
            "sum(case when m.message_status='ACCEPTED' then 1 else 0 end ) as accepted," +
            "sum(case when m.message_status='PRICING' then 1 else 0 end ) as pricing," +
            "sum(case when m.message_status='DESIGNING' then 1 else 0 end ) as designing," +
            "sum(case when m.message_status='CONSULTING' then 1 else 0 end) as consulting," +
            "sum(case when m.message_status='IGNORED' then 1 else 0 end ) as ignored " +
            "from users u left join message m on u.uuid = m.user_uuid " +
            "group by u.first_name, u.last_name, u.uuid, u.created_at " +
            "union all " +
            "select null as fullName, " +
            "null as userId, " +
            "count(*) as total, " +
            "sum(case when m.message_status='ACCEPTED' then 1 else 0 end ) as accepted, " +
            "sum(case when m.message_status='PRICING' then 1 else 0 end) as pricing, " +
            "sum(case when m.message_status='DESIGNING' then 1 else 0 end ) as designing, " +
            "sum(case when m.message_status='CONSULTING' then 1 else 0 end ) as consulting, " +
            "sum(case when m.message_status='IGNORED' then 1 else 0 end ) as ignored " +
            "from message m "
            , nativeQuery = true)
    List<Object[]> getAllByUser();

    @Query(value = "select count(*) from users u join users_roles ur on u.uuid = ur.users_uuid where ur.roles_id=3",nativeQuery = true)
    Integer masterCount();

}
