package ecma.demo.europrintserver.repository;

import ecma.demo.europrintserver.entity.MessageDefault;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface MessageDefaultRepository extends JpaRepository<MessageDefault, UUID> {

    List<MessageDefault> findAllByClientId(UUID clientId);


    @Query(value = "delete from message_default where client_uuid=:clientId", nativeQuery = true)
    void deleteAllByClient_Id(UUID clientId);
}
