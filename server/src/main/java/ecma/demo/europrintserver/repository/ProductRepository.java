package ecma.demo.europrintserver.repository;

import ecma.demo.europrintserver.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface ProductRepository extends JpaRepository<Product, UUID> {

        @Query(value = "select coalesce(sum(amount*price),0) from product where order_uuid=:orderId", nativeQuery = true)
        Double findByOrderId(UUID orderId);

        List<Product> getByOrderId(UUID orderId);
        List<Product> getAllByOrderId(UUID orderId);

        @Query(value = "delete from product where product.order_uuid = :orderId",nativeQuery = true)
        void deleteAllByOrder(UUID orderId);
}
