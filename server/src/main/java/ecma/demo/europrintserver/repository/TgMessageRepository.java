package ecma.demo.europrintserver.repository;

import ecma.demo.europrintserver.entity.TgMessage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface TgMessageRepository extends JpaRepository<TgMessage, UUID> {

    List<TgMessage> findAllByTgId(Long tgId);

}
