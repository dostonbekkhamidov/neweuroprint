package ecma.demo.europrintserver.controller;

import ecma.demo.europrintserver.entity.Company;
import ecma.demo.europrintserver.entity.enums.OrderStatus;
import ecma.demo.europrintserver.payload.ApiResponse;
import ecma.demo.europrintserver.payload.ReqCompany;
import ecma.demo.europrintserver.repository.ClientRepository;
import ecma.demo.europrintserver.repository.CompanyRepository;
import ecma.demo.europrintserver.repository.OrderRepository;
import ecma.demo.europrintserver.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/company")
public class CompanyController {

    @Autowired
    CompanyRepository companyRepository;
    @Autowired
    ClientRepository clientRepository;
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    UserRepository userRepository;

    @GetMapping
    public HttpEntity<?> getCompany() {
        return ResponseEntity.ok(new ApiResponse("success", true, companyRepository.findById(1).get()));
    }

    @PostMapping
    public HttpEntity<?> saveCompany(@RequestBody ReqCompany reqCompany) {
        try {
            Company company = companyRepository.findById(1).get();
            company.setAddress(reqCompany.getAddress());
            company.setEmail(reqCompany.getEmail());
            company.setFacebook(reqCompany.getFacebook());
            company.setInstagram(reqCompany.getInstagram());
            company.setTelegram(reqCompany.getTelegram());
            company.setYoutube(reqCompany.getYoutube());
            company.setPhoneNumber1(reqCompany.getPhoneNumber1());
            company.setPhoneNumber2(reqCompany.getPhoneNumber2());
            company.setPhoneNumber3(reqCompany.getPhoneNumber3());
            company.setAddressRu(reqCompany.getAddressRu());
            company.setCountCustomer(reqCompany.getCustomerCount());
            company.setMasterCount(reqCompany.getMasterCount());
            company.setOrderCount(reqCompany.getOrderCount());
            companyRepository.save(company);
            return ResponseEntity.ok(new ApiResponse("success",true));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.ok(new ApiResponse("error",false));
        }
    }

}
