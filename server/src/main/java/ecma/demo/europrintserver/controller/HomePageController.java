package ecma.demo.europrintserver.controller;

import ecma.demo.europrintserver.entity.Master;
import ecma.demo.europrintserver.entity.Portfolio;
import ecma.demo.europrintserver.payload.ApiResponse;
import ecma.demo.europrintserver.payload.HomePageResponse;
import ecma.demo.europrintserver.repository.MasterRepository;
import ecma.demo.europrintserver.repository.PortfolioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/homepage")
public class HomePageController {

    @Autowired
    PortfolioRepository portfolioRepository;
    @Autowired
    MasterRepository masterRepository;

    @GetMapping
    public HttpEntity<?> getHomePageData(){
        List<Portfolio> portfolios = portfolioRepository.findAll();
        List<Master> masters = masterRepository.findAll();
        HomePageResponse homePageResponse = new HomePageResponse();
        homePageResponse.setPortfolios(portfolios);
        homePageResponse.setMasters(masters);
        return ResponseEntity.ok(new ApiResponse("success",true,homePageResponse));
    }

}
