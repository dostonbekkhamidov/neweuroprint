package ecma.demo.europrintserver.controller;

import ecma.demo.europrintserver.entity.Client;
import ecma.demo.europrintserver.payload.ApiResponse;
import ecma.demo.europrintserver.payload.ResClientReconciliation;
import ecma.demo.europrintserver.payload.ResReconciliation;
import ecma.demo.europrintserver.repository.ReconciliationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/reconciliation")
public class ReconciliationController {

    @Autowired
    ReconciliationRepository reconciliationRepository;

    @GetMapping()
    public HttpEntity<?> getReconciliation(@RequestParam Integer page, @RequestParam Integer size, @RequestParam String search) {
        List<Client> clients = reconciliationRepository.getAllClients(page - 1, size, search);
        List<ResClientReconciliation> clientReconciliations = new ArrayList<>();
        for (Client client : clients) {
            ResClientReconciliation clientReconciliation = new ResClientReconciliation();
            clientReconciliation.setId(client.getId());
            clientReconciliation.setFullName(client.getFullName());
            clientReconciliation.setPhoneNumber(client.getPhoneNumber());
            clientReconciliations.add(clientReconciliation);
        }
        Integer totalElements = reconciliationRepository.getAllClientsTotalElements();
        return ResponseEntity.ok(new ApiResponse("success", true, clientReconciliations, totalElements));
    }

    @GetMapping("/client/{id}")
    public HttpEntity<?> getReconciliationByClient(@PathVariable UUID id, @RequestParam Integer page, @RequestParam Integer size) {
        List<Object[]> objects = reconciliationRepository.getClientTotalReport(id, size, page - 1);
        List<ResReconciliation> reconciliations = new ArrayList<>();
        for (Object[] object : objects) {
            ResReconciliation reconciliation = new ResReconciliation();
            reconciliation.setId(UUID.fromString(object[0].toString()));
            reconciliation.setCreatedAt(object[1].toString());
            reconciliation.setProcess(object[2].toString());
            reconciliation.setName(object[3]!=null ?object[3].toString():null);
            reconciliation.setAmount(object[4] != null ? Integer.parseInt(object[4].toString()) : null);
            reconciliation.setPrice(object[5] != null ? Double.parseDouble(object[5].toString()) : null);
            reconciliation.setPayment(object[6] != null ? Double.parseDouble(object[6].toString()) : null);
            reconciliations.add(reconciliation);
        }
        Integer totalElements = reconciliationRepository.getClientReportTotalElements(id);
        Integer productAmount = reconciliationRepository.getProductAmount(id);
        Double orderPayment = reconciliationRepository.getOrderPayment(id);
        Double payment = reconciliationRepository.getPayment(id);
        Double paymentBack = reconciliationRepository.getPaymentBack(id);
        return ResponseEntity.ok(new ApiResponse(
                "success",
                true,
                reconciliations,
                totalElements,
                productAmount,
                orderPayment,
                payment,
                paymentBack));
    }
}
