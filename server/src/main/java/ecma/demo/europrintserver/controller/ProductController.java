package ecma.demo.europrintserver.controller;

import ecma.demo.europrintserver.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/product")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("{id}")
    public HttpEntity<?> getOrderSum(@PathVariable UUID id){
        return productService.getOrderSum(id);
    }

    @GetMapping("/me/{uuid}")
    public HttpEntity<?> getProducts(@PathVariable UUID uuid){
        return productService.getProductList(uuid);
    }
}
