package ecma.demo.europrintserver.controller;

import ecma.demo.europrintserver.entity.Portfolio;
import ecma.demo.europrintserver.payload.ApiResponse;
import ecma.demo.europrintserver.payload.ReqPortfolio;
import ecma.demo.europrintserver.repository.AttachmentContentRepository;
import ecma.demo.europrintserver.repository.AttachmentRepository;
import ecma.demo.europrintserver.repository.PortfolioRepository;
import ecma.demo.europrintserver.service.AttachmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/portfolio")
public class PortfolioController {

    @Autowired
    PortfolioRepository portfolioRepository;
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    AttachmentService attachmentService;

    @GetMapping
    public HttpEntity<?> getPortfolio() {
        return ResponseEntity.ok(new ApiResponse("true", true, portfolioRepository.findAll()));
    }

    @PostMapping
    public HttpEntity<?> savePortfolio(@ModelAttribute ReqPortfolio reqPortfolio) {
        try {
            portfolioRepository.save(new Portfolio(attachmentRepository.findById(reqPortfolio.getAttachment()).orElseThrow(() -> new ResourceNotFoundException("reqPortfolio"))));
            return ResponseEntity.ok(new ApiResponse("success", true));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.ok(new ApiResponse("false", false));
        }
    }

    @DeleteMapping("{id}")
    public HttpEntity<?> deletePortfolio(@PathVariable UUID id) {
        try {
            Portfolio portfolio = portfolioRepository.findById(id).get();
            portfolioRepository.deleteById(id);
            attachmentService.delete(portfolio.getAttachment().getId());
            return ResponseEntity.ok(new ApiResponse("success",true));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.ok(new ApiResponse("error",false));
        }
    }
}

