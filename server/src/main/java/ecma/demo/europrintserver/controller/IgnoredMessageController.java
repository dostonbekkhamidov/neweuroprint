package ecma.demo.europrintserver.controller;

import ecma.demo.europrintserver.entity.IgnoredMessage;
import ecma.demo.europrintserver.entity.Message;
import ecma.demo.europrintserver.payload.ApiResponse;
import ecma.demo.europrintserver.repository.IgnoredMessageRepository;
import ecma.demo.europrintserver.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/ignored")
public class IgnoredMessageController {

    @Autowired
    IgnoredMessageRepository ignoredMessageRepository;
    @Autowired
    MessageRepository messageRepository;

    @PostMapping
    public HttpEntity<?> save(@RequestParam String reason, @RequestParam String text, @RequestParam UUID id) {
        Message message = messageRepository.findById(id).get();
        ignoredMessageRepository.save(new IgnoredMessage(reason, text, message));
        return ResponseEntity.ok(new ApiResponse("success", true));
    }

}
