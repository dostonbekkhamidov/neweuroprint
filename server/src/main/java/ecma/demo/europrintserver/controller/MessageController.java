package ecma.demo.europrintserver.controller;

import ecma.demo.europrintserver.entity.User;
import ecma.demo.europrintserver.entity.enums.MessageStatus;
import ecma.demo.europrintserver.security.CurrentUser;
import ecma.demo.europrintserver.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/message")
public class MessageController {

    @Autowired
    MessageService messageService;

    @GetMapping
    public HttpEntity<?> getAll(@CurrentUser User user) {
        return messageService.getAll(user);
    }

    @GetMapping("/me")
    public HttpEntity<?> getMessageByManager( @RequestParam String status, @RequestParam Integer size, @CurrentUser User user) {
        return messageService.getAllMessagesByStatus(user,size, MessageStatus.valueOf(status));
    }

    @PatchMapping("{id}")
    public HttpEntity<?> updateDisabled(@PathVariable UUID id, @CurrentUser User user) {
        return messageService.changeDisabled(id, user);
    }

    @PatchMapping("/status/{id}")
    public HttpEntity<?> update(@PathVariable UUID id, @RequestParam String status, @CurrentUser User user) {
        return messageService.updateStatus(id, status, user);
    }
}
