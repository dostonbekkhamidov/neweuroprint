package ecma.demo.europrintserver.controller;

import ecma.demo.europrintserver.payload.ReqChat;
import ecma.demo.europrintserver.repository.AttachmentContentRepository;
import ecma.demo.europrintserver.repository.AttachmentRepository;
import ecma.demo.europrintserver.service.ChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping("/api/chat")
public class ChatController {

    @Autowired
    ChatService chatService;
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    AttachmentContentRepository attachmentContentRepository;

    @GetMapping
    public HttpEntity<?> getAll(@RequestParam UUID messageId){
        return chatService.getAll(messageId);
    }

    @PostMapping
    public HttpEntity<?> saveChatWithText(@RequestBody ReqChat reqChat){
        return chatService.saveChatWithText(reqChat);
    }

    @PostMapping("/file")
    public HttpEntity<?> saveChatWithFile(MultipartHttpServletRequest request) throws IOException, TelegramApiException {
       return chatService.saveChatWithFile(request);
    }
}
