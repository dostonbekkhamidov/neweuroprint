package ecma.demo.europrintserver.controller;

import ecma.demo.europrintserver.payload.ReqOrder;
import ecma.demo.europrintserver.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/order")
public class OrderController {

    @Autowired
    OrderService orderService;

    @GetMapping
    public HttpEntity<?> getOrders(@RequestParam String status, @RequestParam Integer page, @RequestParam Integer size) {
        return orderService.getOrders(status, page - 1, size);
    }

    @GetMapping("{id}")
    public HttpEntity<?> getOne(@PathVariable UUID id) {
        return orderService.getOne(id);
    }

    @GetMapping("/reportTop")
    public HttpEntity<?> getReportTopPage() {
        return orderService.getReportTopPage();
    }

    @GetMapping("/reportBelow")
    public HttpEntity<?> getReportBelowPage(@RequestParam String month) {
        return orderService.getReportBelowPage(month);
    }

    @PostMapping
    public HttpEntity<?> save(@RequestBody ReqOrder reqOrder) {
        return orderService.save(reqOrder);
    }

    @PatchMapping("{id}")
    public HttpEntity<?> updateOrderStatus(@PathVariable UUID id, @RequestParam String status) {
        return orderService.updateOrderStatus(id, status);
    }

    @PatchMapping("edit/{id}")
    public HttpEntity<?> updateOrder(@RequestBody ReqOrder reqOrder, @PathVariable UUID id) {
        return orderService.updateOrder(reqOrder, id);
    }

}
