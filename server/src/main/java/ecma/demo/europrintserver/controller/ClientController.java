package ecma.demo.europrintserver.controller;

import ecma.demo.europrintserver.payload.ApiResponse;
import ecma.demo.europrintserver.payload.ReqClient;
import ecma.demo.europrintserver.service.ClientService;
import org.omg.CORBA.INTERNAL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Path;
import java.util.UUID;

@RestController
@RequestMapping("/api/client")
public class ClientController {

    @Autowired
    ClientService clientService;

    @GetMapping
    public HttpEntity<?> get(@RequestParam String search, @RequestParam Integer page, @RequestParam Integer size) {
        return clientService.get(search,page,size);
    }

    @GetMapping("{id}")
    public HttpEntity<?> getHistory(@PathVariable UUID id){
        return clientService.getHistoryByManager(id);
    }

    @GetMapping("/balance/{id}")
    public HttpEntity<?> getBalance(@PathVariable UUID id){
        return clientService.getClientBalance(id);
    }

    @PostMapping
    public HttpEntity<?> save(@RequestBody ReqClient reqClient){
        return clientService.save(reqClient);
    }

    @PatchMapping("{id}")
    public HttpEntity<?> update(@PathVariable UUID id,@RequestBody ReqClient reqClient){
        return clientService.edit(id,reqClient);
    }

    @DeleteMapping("{id}")
    public HttpEntity<?> remove(@PathVariable UUID id){
        return clientService.delete(id);
    }

}
