package ecma.demo.europrintserver.controller;

import ecma.demo.europrintserver.entity.User;
import ecma.demo.europrintserver.payload.ApiResponse;
import ecma.demo.europrintserver.payload.ReqUser;
import ecma.demo.europrintserver.repository.UserRepository;
import ecma.demo.europrintserver.security.CurrentUser;
import ecma.demo.europrintserver.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

//@CrossOrigin("*")
@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userService;

    @GetMapping("/me")
    public HttpEntity<?> getUser(@CurrentUser User user) {
        if (user == null) {
            return ResponseEntity.ok(new ApiResponse("error", false));
        } else {
            return ResponseEntity.ok(new ApiResponse("success", true, user));
        }
    }

    @GetMapping
    public HttpEntity<?> getUsersForDirector() {
        return ResponseEntity.ok(new ApiResponse("ok", true, userRepository.findAll()));
    }

    @GetMapping("/all")
    public HttpEntity<?> getAllUsersByMessage(){
        return userService.getAllUsersByMessage();
    }

    @GetMapping("/ignored/{id}")
    public HttpEntity<?> getAllByIgnored(@PathVariable UUID id){
        return userService.getAllByIgnored(id);
    }

    @GetMapping("{id}")
    public HttpEntity<?> getOne(@PathVariable UUID id){
        return userService.getOne(id);
    }

    @PatchMapping("{id}")
    public HttpEntity<?> editUser(@RequestBody ReqUser reqUser, @PathVariable UUID id) {
        return userService.updateUser(reqUser, id);
    }

    @PostMapping("{userId}")
    public HttpEntity<?> checkUserPassword(@PathVariable UUID userId, @RequestParam String password) {
        return userService.checkUserPassword(userId, password);
    }

    @PostMapping
    public HttpEntity<?> saveUser(@RequestBody ReqUser user) {
        return userService.saveUser(user);
    }

    @DeleteMapping("{id}")
    public HttpEntity<?> deleteUser(@PathVariable UUID id) {
        userRepository.deleteById(id);
        return ResponseEntity.ok(new ApiResponse("ok", true));
    }
}
