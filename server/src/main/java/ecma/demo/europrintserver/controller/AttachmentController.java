package ecma.demo.europrintserver.controller;

import ecma.demo.europrintserver.entity.Attachment;
import ecma.demo.europrintserver.repository.AttachmentContentRepository;
import ecma.demo.europrintserver.repository.AttachmentRepository;
import ecma.demo.europrintserver.service.AttachmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;


import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

@RestController
@RequestMapping("api/file")
public class AttachmentController {

    @Autowired
    AttachmentService attachmentService;

    @Autowired
    AttachmentContentRepository attachmentContentRepository;

    @Autowired
    AttachmentRepository attachmentRepository;

    @PostMapping("/save")
    public Attachment save(MultipartHttpServletRequest request) {
        return attachmentService.saveFile(request);
    }

    @GetMapping("/get/{id}")
    public void getFile(HttpServletResponse response, @PathVariable String id) {
        attachmentService.getFile(response, id);
    }

    @DeleteMapping("{id}")
    public HttpEntity<?> delete(@PathVariable String id){
        return attachmentService.delete(UUID.fromString(id));
    }

}
