package ecma.demo.europrintserver.controller;

import ecma.demo.europrintserver.entity.Master;
import ecma.demo.europrintserver.exception.ResourceNotFoundException;
import ecma.demo.europrintserver.payload.ApiResponse;
import ecma.demo.europrintserver.payload.ReqEditMAster;
import ecma.demo.europrintserver.payload.ReqMaster;
import ecma.demo.europrintserver.repository.AttachmentRepository;
import ecma.demo.europrintserver.repository.MasterRepository;
import ecma.demo.europrintserver.service.MasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/master")
public class MasterController {

    @Autowired
    MasterRepository masterRepository;
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    MasterService masterService;

    @GetMapping
    public HttpEntity<?> getMasters() {
        return ResponseEntity.ok(new ApiResponse("success", true, masterRepository.findAllByCreatedAtAsc()));
    }

    @PostMapping
    public HttpEntity<?> saveMaster(@RequestBody ReqMaster reqMaster) {
        return ResponseEntity.ok(new ApiResponse("success", true,
                masterRepository.save(new Master(
                        reqMaster.getName(),
                        reqMaster.getDescription(),
                        attachmentRepository.findById(reqMaster.getAttachment()).
                                orElseThrow(() -> new ResourceNotFoundException("master", "id", reqMaster)),
                        reqMaster.getActive()))));
    }

    @PutMapping
    public HttpEntity<?> editMaster(@RequestBody ReqEditMAster reqEditMAster) {
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(
                masterService.editingMaster(reqEditMAster));
    }

    @DeleteMapping("{id}")
    public HttpEntity<?> deletePayment(@PathVariable UUID id) {
        return masterService.delete(id);
    }

}
