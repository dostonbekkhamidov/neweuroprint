package ecma.demo.europrintserver.controller;

import ecma.demo.europrintserver.payload.ReqPayment;
import ecma.demo.europrintserver.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/payment")
public class PaymentController {

    @Autowired
    PaymentService paymentService;

    @GetMapping("{clientId}")
    public HttpEntity<?> get(@PathVariable UUID clientId,@RequestParam Integer page, @RequestParam Integer size){
        return paymentService.get(clientId,page,size);
    }

    @PostMapping
    public HttpEntity<?> save(@RequestBody ReqPayment reqPayment){
        return paymentService.save(reqPayment);
    }

}
